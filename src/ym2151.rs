use crate::config::Config;
use crate::midi_shim::{
	db_to_midi_vol, MIDIShim, MIDI_MODULATOR_WHEEL, MIDI_PAN, MIDI_PAN_CENTER, MIDI_PAN_LEFT,
	MIDI_PAN_RIGHT, MIDI_VOLUME,
};
use crate::vgm2mid::do_note_on;
use crate::ym3812::ym3812_vol_to_db;
use midly::MidiMessage::{Controller, ProgramChange};
use midly::TrackEventKind::Midi;

// YM2151 Register Constants
#[allow(dead_code)]
const YM2151_TEST: u8 = 0x01;
const YM2151_KEY_ON: u8 = 0x08;
const YM2151_NOISE: u8 = 0x0F;
const YM2151_CLK_A_MSB: u8 = 0x10;
const YM2151_CLK_A_LSB: u8 = 0x11;
const YM2151_CLK_B: u8 = 0x12;
const YM2151_CSM_IRQ_RESET_EN_TIMER: u8 = 0x14;
const YM2151_LFO_FREQ: u8 = 0x18;
const YM2151_PMD_AMD: u8 = 0x19;
const YM2151_CT1_CT2_WAVEFORM: u8 = 0x1B;
const YM2151_PAN_FB_CONNECTION: u8 = 0x20;
const YM2151_PAN_FB_CONNECTION_END: u8 = YM2151_PAN_FB_CONNECTION + 0x07;
const YM2151_BLK_FNUM: u8 = 0x28;
const YM2151_BLK_FNUM_END: u8 = YM2151_BLK_FNUM + 0x07;
const YM2151_KF: u8 = 0x30;
const YM2151_KF_END: u8 = YM2151_KF + 0x07;
const YM2151_PMS_AMS: u8 = 0x38;
const YM2151_PMS_AMS_END: u8 = YM2151_PMS_AMS + 0x07;
const YM2151_DT1_MUL: u8 = 0x40;
const YM2151_DT1_MUL_END: u8 = YM2151_DT1_MUL + 0x1F;
const YM2151_TL: u8 = 0x60;
const YM2151_TL_END: u8 = YM2151_TL + 0x1F;
#[allow(dead_code)]
const YM2151_KS_AR: u8 = 0x80;
#[allow(dead_code)]
const YM2151_LFO_AM_EN_D1R: u8 = 0xA0;
#[allow(dead_code)]
const YM2151_DT2_D2R: u8 = 0xC0;
#[allow(dead_code)]
const YM2151_D1L_RR: u8 = 0xE0;

fn ym2151_fnum_to_midi(mut fnum: u8, mut block: u8) -> u8 {
	let mut note_val: u8;

	//if FNum > 0xF {
	//	MsgBox "FNumToHz (2151): invalid fnum " & Format$(FNum)
	//}
	if block > 0x7 {
		block = 0x7
	}

	fnum = fnum & 0xF;
	if !((fnum & 0x3) == 0x3) {
		note_val = (fnum & 0xC) / 0x4;
		note_val = note_val * 3 + (fnum & 0x3);
		note_val = 61 + note_val;
	} else {
		note_val = 0xFF
	}

	if note_val == 0xFF {
		0xFF
	} else {
		note_val + (block - 0x4) * 12
		//if YM2151FNumToMidi = 0x0 { Stop
	}
}

pub(crate) struct YM2151State {
	slot: [u8; 8],
	//	DT(5, 3) As Byte, MULTI(5, 3) As Byte
	tl: [[u8; 3]; 8],
	//KF As Byte
	//	KS(5, 3) As Byte, AR(5, 3) As Byte
	//	DR: [u8; 5, 3],
	//	SR: [u8; 5, 3],
	//	SL(5, 3) As Byte, RR(5, 3) As Byte
	fnum_lsb: [u16; 8],
	fnum_msb: [u8; 8],
	block: [u8; 8],
	fnum_1: [u32; 8],
	fnum_2: [u32; 8],
	note_1: [f64; 8],
	note_2: [f64; 8],
	feedback: [u8; 8],
	connection: [u8; 8],
	midi_instr: [u8; 8],
	midi_note: [u8; 8],
	midi_wheel: [u16; 8],
	midi_volume: [u8; 8],
	midi_pan: [u8; 8],
	note_on_1: [bool; 8],
	note_on_2: [bool; 8],
}

impl Default for YM2151State {
	fn default() -> Self {
		YM2151State {
			slot: [0; 8],
			tl: [[0; 3]; 8],
			fnum_lsb: [0; 8],
			fnum_msb: [0; 8],
			block: [0; 8],
			fnum_1: [0; 8],
			fnum_2: [0; 8],
			note_1: [0xFF.into(); 8],
			note_2: [0xFF.into(); 8],
			feedback: [0; 8],
			connection: [0; 8],
			midi_instr: [0xFF; 8],
			midi_note: [0xFF.into(); 8],
			midi_wheel: [0x8000; 8],
			midi_volume: [0xFF; 8],
			midi_pan: [0; 8],
			note_on_1: [false; 8],
			note_on_2: [false; 8],
		}
	}
}

//FIXME: This subroutine used a lot of static state. It needs to be re-written to something more sane.
pub(crate) fn ym2151_command_handle(
	register: u8,
	data: u8,
	state: &mut YM2151State,
	config: &Config,
	midi: &mut MIDIShim,
) {
	/*if crate::vgm2mid::VARIABLES_CLEAR_YM2151 {
		/*Erase Slot: Erase TL: Erase FNum_LSB: Erase FNum_MSB: Erase Block
		Erase FNum_1: Erase FNum_2: Erase Note_1: Erase Note_2
		Erase Feedback: //Erase Connection
		Erase MIDIInstr: Erase MIDINote: Erase MIDIWheel: Erase MIDIVolume
		Erase NoteOn_1: Erase NoteOn_2*/
		crate::vgm2mid::VARIABLES_CLEAR_YM2151 = 0;
		for channel in 0x0..0x7 {
			state.note_1[channel as usize] = 255.0; //FIXME: wtf? This was originally 0xFF on a Double
			state.note_2[channel as usize] = 255.0;
			state.note_on_2[channel as usize] = 0x0;
			state.midi_instr[channel as usize] = 0xFF;
			state.midi_note[channel as usize] = 0xFF;
			state.midi_wheel[channel as usize] = 0x8000;
			state.midi_volume[channel as usize] = 0xFF;
		}
	}*/

	match register {
		YM2151_KEY_ON => {
			fun_name(data, config, state, midi);
		},
		YM2151_NOISE => (), // NOISE: NE, NFRQ
		//nen = if(Data & 0x80, 1, 0)
		//nfrq = Data & 0x31
		YM2151_CLK_A_MSB => (),
		YM2151_CLK_A_LSB => (),
		YM2151_CLK_B => (),
		YM2151_CSM_IRQ_RESET_EN_TIMER => (),
		YM2151_LFO_FREQ => (),
		YM2151_PMD_AMD => (),
		//depth = Data & 0x7F
		//ispm = if(Data & 0x80, 1, 0)
		YM2151_CT1_CT2_WAVEFORM => (),
		//w = Data & 0x3	// W: 0 - ramp, 1 - sq, 2 - tri, 3 - noise
		YM2151_PAN_FB_CONNECTION..=YM2151_PAN_FB_CONNECTION_END => {
			process_pan_feedback(register, config, data, state, midi);
		},
		YM2151_BLK_FNUM..=YM2151_BLK_FNUM_END => {
			process_fnum(register, config, state, data, midi);
		},
		YM2151_KF..=YM2151_KF_END => {
			process_kf(register, config, state, data, midi);
		},
		YM2151_PMS_AMS..=YM2151_PMS_AMS_END => {
			process_modulation(register, data, midi);
		},
		YM2151_DT1_MUL..=YM2151_DT1_MUL_END => {
			//let channel = (register & 0x3F) / 0x8;
			//DT1 = Fix(Data / 0x10) & 0x7
			//MUl = Data & 0xF
		},
		YM2151_TL..=YM2151_TL_END => {
			process_total_level(register, state, data, config, midi);
		},
		/*YM2151_KS_AR..=YM2151_KS_AR + 0x1F
			channel = Fix((register & 0x3F) / 0x8)
			//KS = Fix(Data / 0x40) & 0x3
			//AR = Data & 0x1F
		YM2151_LFO_AM_EN_D1R..=YM2151_LFO_AM_EN_D1R + 0x1F
			channel = Fix((register & 0x3F) / 0x8)
			//AMS_EN = if(Data & 0x80, 1, 0)
			//D1R = Data & 0x1F
		YM2151_DT2_D2R..=YM2151_DT2_D2R + 0x1F
			channel = Fix((register & 0x3F) / 0x8)
			//DT2 = Fix(Data / 0x40) & 0x3
			//D2R = Data & 0x1F
		YM2151_D1L_RR..=YM2151_D1L_RR + 0x1F
			channel = Fix((register & 0x1F) / 0x4)
			//D1L = Fix(Data / 0x10) & 0xF
			//RR = Data & 0xF
			//Abort("YM2151 ADSR - SL " & D1L & " RR " & RR);*/
		_ => (),
	}
}

fn process_total_level(
	register: u8,
	state: &mut YM2151State,
	data: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let channel = register & 0x7;
	let op = (register & 0x1F) / 0x8;
	state.tl[channel as usize][op as usize] = data & 0x7F;
	if op < 3 {
		return;
	}
	if config.ym2151_vol_disabled[channel as usize] {
		return;
	}
	let temp_byte = db_to_midi_vol(ym3812_vol_to_db(state.tl[channel as usize][3]) / 4.0);

	//match state.connection[channel as usize]
	//0, 1, 2, 3
	//4
	//	TempLng = (CLng(state.tl[channel as usize][1]) + state.tl[channel as usize][3]) / 2
	//5, 6
	//	TempLng = (CLng(state.tl[channel as usize][1]) + state.tl[channel as usize][2] + state.tl[channel as usize][3]) / 3
	//7
	//	TempLng = (CLng(state.tl[channel as usize][0]) + state.tl[channel as usize][1] + state.tl[channel as usize][2] + state.tl[channel as usize][3]) / 4
	//}

	if temp_byte != state.midi_volume[channel as usize] {
		state.midi_volume[channel as usize] = temp_byte;
		midi.event_write(Midi {
			channel: channel.into(),
			message: Controller {
				controller: MIDI_VOLUME,
				value: state.midi_volume[channel as usize].into(),
			},
		});
	}
}

fn process_modulation(register: u8, data: u8, midi: &mut MIDIShim) {
	let channel = register & 0x7;
	//PMS = Fix(Data / 0x10) & 0x7
	//AMS = Data & 0x3
	let temp_double = ((data & 0x70) as f32) / (0x70 as f32);
	let temp = (temp_double * temp_double * (0x7F as f32)) as u8;
	midi.event_write(Midi {
		channel: channel.into(),
		message: Controller {
			controller: MIDI_MODULATOR_WHEEL,
			value: temp.into(),
		},
	});
}

fn process_kf(
	register: u8,
	config: &Config,
	state: &mut YM2151State,
	data: u8,
	midi: &mut MIDIShim,
) {
	let channel = register & 0x7;
	if config.ym2151_ch_disabled[channel as usize] {
		return;
	}
	state.fnum_lsb[channel as usize] = ((data / 0x4) & 0x3F) as u16;
	state.note_1[channel as usize] = state.note_2[channel as usize];
	state.note_2[channel as usize] = (ym2151_fnum_to_midi(
		state.fnum_2[channel as usize] as u8,
		state.block[channel as usize],
	) + (state.fnum_lsb[channel as usize] / 0x40) as u8)
		.into();

	// Key Fraction goes from 0x00 to 0xFC
	// 0x00 is no change, 0x100 would be a full semitone

	//TempLng = MIDI_PITCHWHEEL_CENTER + state.fnum_lsb[channel as usize] * 0x40	 //(0x1000 / 0x40)
	//if TempLng != state.midi_wheel[channel as usize] {
	//	state.midi_wheel[channel as usize] = TempLng
	//	midi.event_write(Midi { channel: channel, message: PitchBend { bend: state.midi_wheel[channel as usize] }});
	//}
	if state.note_on_2[channel as usize] {
		do_note_on(
			state.note_1[channel as usize],
			state.note_2[channel as usize],
			channel,
			&mut state.midi_note[channel as usize],
			&mut state.midi_wheel[channel as usize],
			None,
			None,
			config,
			midi,
		);
	}
}

fn process_fnum(
	register: u8,
	config: &Config,
	state: &mut YM2151State,
	data: u8,
	midi: &mut MIDIShim,
) {
	let channel = register & 0x7;
	if config.ym2151_ch_disabled[channel as usize] {
		return;
	}
	state.block[channel as usize] = (data / 0x10) & 0x7;
	state.fnum_msb[channel as usize] = data & 0xF;
	state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
	state.fnum_2[channel as usize] = state.fnum_msb[channel as usize] as u32;
	state.note_1[channel as usize] = state.note_2[channel as usize];
	state.note_2[channel as usize] = ((ym2151_fnum_to_midi(
		state.fnum_2[channel as usize] as u8,
		state.block[channel as usize],
	) + state.fnum_lsb[channel as usize] as u8) as f64)
		/ (0x40 as f64);

	//FIXME: fnum_2 really shouldn't be a long
	//if state.note_on_2[channel as usize] = 1 | NOTE_ON_MODE & 0x2 != 0 {
	//	if state.note_2[channel as usize] != state.note_1[channel as usize] | (NOTE_ON_MODE & 0x2) = 0x2 {
	//		midi.event_write(Midi { channel: channel, message: NoteOff { key: state.note_1[channel as usize], vel: 0x0 }});
	//		midi.event_write(Midi { channel: channel, message: NoteOn { key: state.note_2[channel as usize], vel: 0x7F }});
	//	}
	//}
	if state.note_on_2[channel as usize] {
		do_note_on(
			state.note_1[channel as usize],
			state.note_2[channel as usize],
			channel as u8,
			&mut state.midi_note[channel as usize],
			&mut state.midi_wheel[channel as usize],
			None,
			None,
			config,
			midi,
		);
	}
}

fn process_pan_feedback(
	register: u8,
	config: &Config,
	data: u8,
	state: &mut YM2151State,
	midi: &mut MIDIShim,
) {
	let channel = register & 0x7;
	if config.ym2151_ch_disabled[channel as usize] {
		return;
	}
	if !config.ym2151_pan_disabled[channel as usize] {
		match (data / 0x40) & 0x3 {
			0x1 => {
				if state.midi_pan[channel as usize] != MIDI_PAN_LEFT || true {
					//FIXME: wtf?! why would *anyone* write this without leaving a comment.
					state.midi_pan[channel as usize] = MIDI_PAN_LEFT.into();
					midi.event_write(
						Midi {
							channel: channel.into(),
							message: Controller {
								controller: MIDI_PAN,
								value: state.midi_pan
									[channel as usize]
									.into(),
							},
						}
						.clone(),
					);
				}
			},
			0x2 => {
				if state.midi_pan[channel as usize] != MIDI_PAN_RIGHT || true {
					state.midi_pan[channel as usize] = MIDI_PAN_RIGHT.into();
					midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_PAN,
							value: state.midi_pan[channel as usize]
								.into(),
						},
					});
				}
			},
			0x3 | 0x0 => {
				if state.midi_pan[channel as usize] != MIDI_PAN_CENTER || true {
					state.midi_pan[channel as usize] = MIDI_PAN_CENTER.into();
					midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_PAN,
							value: state.midi_pan[channel as usize]
								.into(),
						},
					});
				}
			},
			_ => panic!(),
		}
	}
	state.feedback[channel as usize] = (data / 0x8) & 0x7;
	state.connection[channel as usize] = data & 0x7;

	if !config.ym2151_prog_disabled[channel as usize] {
		state.midi_instr[channel as usize] = data & 0x3F;
		midi.event_write(Midi {
			channel: channel.into(),
			message: ProgramChange {
				program: state.midi_instr[channel as usize].into(),
			},
		});
	}
}

fn fun_name(data: u8, config: &Config, state: &mut YM2151State, midi: &mut MIDIShim) {
	let channel = data & 0x7;
	if config.ym2151_ch_disabled[channel as usize] {
		return;
	}
	state.slot[channel as usize] = (data / 0x8) & 0xF;
	state.note_on_1[channel as usize] = state.note_on_2[channel as usize];
	state.note_on_2[channel as usize] = state.slot[channel as usize] > 0;

	if
	/*NOTE_ON_MODE & 0x1*/
	true {
		//FIXME: wtf?
		// This is a const. wtf?
		if (state.note_on_2[channel as usize] != state.note_on_1[channel as usize])
			&& (state.note_2[channel as usize] != 0.0)
		{
			if !state.note_on_2[channel as usize] {
				//midi.event_write(Midi { channel: channel, message: NoteOff { key: state.note_2[channel as usize], vel: 0x0 }});
				do_note_on(
					state.note_1[channel as usize],
					0xFF as f64,
					channel as u8,
					&mut state.midi_note[channel as usize],
					&mut state.midi_wheel[channel as usize],
					Some(255),
					None,
					config,
					midi,
				);
			} else {
				//midi.event_write(Midi { channel: channel, message: NoteOn { key: state.note_2[channel as usize], vel: 0x7F }});
				do_note_on(
					state.note_1[channel as usize],
					state.note_2[channel as usize],
					channel as u8,
					&mut state.midi_note[channel as usize],
					&mut state.midi_wheel[channel as usize],
					Some(255),
					None,
					config,
					midi,
				);
			}
		}
	}
}
