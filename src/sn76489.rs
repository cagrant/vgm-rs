use crate::config::Config;
use crate::midi_shim::MIDIShim;
use crate::midi_shim::{
	db_to_midi_vol, MIDI_PAN, MIDI_PAN_CENTER, MIDI_PAN_LEFT, MIDI_PAN_RIGHT, MIDI_VOLUME,
	MIDI_VOLUME_MAX,
};
use crate::vgm2mid::{do_note_on, note};
use midly::MidiMessage::Controller;
use midly::MidiMessage::NoteOff;
use midly::MidiMessage::NoteOn;
use midly::TrackEventKind::Midi;

pub(crate) const SN76489_LATCH: u8 = 0x80; //128
pub(crate) const SN76489_CHANNEL_SELECT: u8 = SN76489_LATCH | 0x70; //240
pub(crate) const SN76489_TONE_1: u8 = SN76489_LATCH | 0x0; //128
pub(crate) const SN76489_TONE_2: u8 = SN76489_LATCH | 0x20; //160
pub(crate) const SN76489_TONE_3: u8 = SN76489_LATCH | 0x40; //192
pub(crate) const SN76489_NOISE: u8 = SN76489_LATCH | 0x60; //224

const SN76489_CHN_MASK: u8 = 0x60; //112

#[allow(dead_code)]
const SN76489_FB_PERIODIC: u8 = 0x00;
#[allow(dead_code)]
const SN76489_FB_WHITE: u8 = 0x01;

#[allow(dead_code)]
const SN76489_CLOCK_SOURCE_HALF: u8 = 0x00;
#[allow(dead_code)]
const SN76489_CLOCK_SOURCE_FOURTH: u8 = 0x01;
#[allow(dead_code)]
const SN76489_CLOCK_SOURCE_EIGHTH: u8 = 0x02;
#[allow(dead_code)]
const SN76489_CLOCK_SOURCE_TONE_3: u8 = 0x03;

const SN76489_ATTENUATOR_1: u8 = SN76489_LATCH | 0x10; //144
const SN76489_ATTENUATOR_2: u8 = SN76489_LATCH | 0x30; //176
const SN76489_ATTENUATOR_3: u8 = SN76489_LATCH | 0x50; //208
const SN76489_ATTENUATOR_NOISE: u8 = SN76489_LATCH | 0x70; //240
#[allow(dead_code)]
const SN76489_ATTENUATION_MIN: u8 = 0x00;
const SN76489_ATTENUATION_MAX: u8 = 0x0F;

pub(crate) const MIDI_CHANNEL_SN76489_BASE: u8 = 0x0A;
// used for Note On/Off-Detection
//Public SN76489_LastVol(0x0..=0x7) As Byte

//FIXME: This won't behave well if fnum is small.
pub(crate) fn hz_sn76489(fnum: u32, clock: u32) -> f64 {
	if fnum == 0 {
		0.0
	} else {
		(clock as f64 / 32.0) / fnum as f64
	}
}

pub(crate) struct SN76489State {
	fnum_1: [u32; 8],
	fnum_2: [u32; 8],
	hz_1: [f64; 8],
	hz_2: [f64; 8],
	note_1: [f64; 8],
	note_2: [f64; 8],
	feedback: [u8; 2],
	clock_source: [u8; 2],
	attenuation_1: [u8; 8],
	attenuation_2: [u8; 8],
	#[allow(dead_code)]
	connection: [u8; 8],
	#[allow(dead_code)]
	midi_instr: [u8; 8],
	midi_note: [u8; 8],
	midi_wheel: [u16; 8],
	midi_volume: u8,
	#[allow(dead_code)]
	midi_pan: [u8; 8],
	note_on_1: [bool; 8],
	note_on_2: [bool; 8],
	gg_pan: [[u8; 5]; 2],
	pub num: u8,
	pub note_delay: [u32; 8],
}

impl Default for SN76489State {
	fn default() -> Self {
		SN76489State {
			fnum_1: [0; 8],
			fnum_2: [0; 8],
			hz_1: [0.0; 8],
			hz_2: [0.0; 8],
			note_1: [0xFF.into(); 8],
			note_2: [0xFF.into(); 8],
			feedback: [0; 2],
			clock_source: [0; 2],
			attenuation_1: [0; 8],
			attenuation_2: [0xFF; 8],
			connection: [0; 8],
			midi_instr: [0; 8],
			midi_note: [0xFF; 8],
			midi_wheel: [0x8000; 8],
			midi_volume: 0,
			midi_pan: [0; 8],
			note_on_1: [false; 8],
			note_on_2: [false; 8],
			gg_pan: [[0, 0, 0, 0, 0xFF]; 2],
			num: 0,
			note_delay: [0; 8],
		}
	}
}

pub(crate) struct SN76489<'a> {
	pub(crate) state: SN76489State,
	config: &'a Config,
}

impl<'a> SN76489<'a> {
	pub(crate) fn new<'b: 'a>(config: &'b Config, opt_state: Option<SN76489State>) -> Self {
		SN76489 {
			state: opt_state.unwrap_or(Default::default()),
			config,
		}
	}

	pub(crate) fn command_handle(
		&mut self,
		msb: u8,
		lsb: u8,
		clock: u32,
		t6w28_sn76489: bool,
		midi: &mut MIDIShim,
	) {
		if msb & 0x80 == 0x0 {
			return;
		}

		let mut channel = (msb & SN76489_CHN_MASK) / 0x20;

		if channel < 0x3 {
			if self.config.sn76489_ch_disabled[channel as usize] {
				return;
			}
		} else {
			//if CH = 0x3{
			if self.config.sn76489_noise_disabled {
				return;
			}
		}

		channel = channel | (self.state.num << 2);
		let mut midi_channel = MIDI_CHANNEL_SN76489_BASE + (channel & 0x03);
		if self.state.num == 0x01 {
			midi_channel -= MIDI_CHANNEL_SN76489_BASE
		}

		match msb & SN76489_CHANNEL_SELECT {
			SN76489_TONE_1 | SN76489_TONE_2 | SN76489_TONE_3 => self
				.process_tone_channels(
					msb,
					lsb,
					channel as usize,
					midi_channel,
					clock,
					t6w28_sn76489,
					midi,
				),
			SN76489_NOISE => self.process_noise_channel(lsb, channel, clock),
			SN76489_ATTENUATOR_1 | SN76489_ATTENUATOR_2 | SN76489_ATTENUATOR_3 => {
				self.process_tone_attenuation(channel, lsb, midi_channel, midi)
			},
			SN76489_ATTENUATOR_NOISE => {
				self.process_noise_attenuation(channel, lsb, midi_channel, midi)
			},
			_ => panic!(),
		}
	}

	fn process_tone_channels(
		&mut self,
		msb: u8,
		lsb: u8,
		mut channel: usize,
		mut midi_channel: u8,
		clock: u32,
		t6w28_sn76489: bool,
		midi: &mut MIDIShim,
	) {
		self.state.fnum_1[channel] = self.state.fnum_2[channel];
		self.state.fnum_2[channel] =
			((lsb as u32 & 0x3F) << 4) + (msb & 0x0F) as u32;

		self.state.hz_1[channel] = self.state.hz_2[channel];
		self.state.hz_2[channel] =
			hz_sn76489(self.state.fnum_2[channel].into(), clock);

		if !(t6w28_sn76489 && self.state.num > 0x00) {
			let mut temp_note = note(self.state.hz_2[channel]);

			self.state.note_1[channel] = self.state.note_2[channel];
			if temp_note >= 0x80.into() {
				//TempNote = 0x7F - self.state.fnum_2[channel]
				temp_note = 0xFF.into();
			}
			self.state.note_2[channel] = temp_note;
			if t6w28_sn76489 {
				self.state.note_1[0x04 + channel] =
					self.state.note_2[0x04 + channel];
				self.state.note_2[0x04 + channel] = temp_note;
			}
		} else {
			//if T6W28_SN76489 & SN76489_NUM > 0x0{
			let mut temp_note = note(self.state.hz_2[channel] / 2.0);
			if (channel & 0x03) == 0x02 {
				if temp_note < 0xFF.into() {
					temp_note = temp_note - 24.0; // - 2 Octaves
					if temp_note >= 0x80.into() {
						temp_note = 0x7F.into();
					}
				}
				self.state.note_2[0x3] = temp_note;
				self.state.note_2[0x7] = temp_note;
			}
			return;
		}

		if self.state.fnum_2[channel] == 0 {
			if self.state.note_on_2[channel] {
				if self.state.midi_note[channel] < 0xFF {
					do_note_on(
						self.state.note_1[channel],
						self.state.note_2[channel],
						midi_channel,
						&mut self.state.midi_note[channel],
						&mut self.state.midi_wheel[channel],
						None,
						None,
						self.config,
						midi,
					);
					self.state.midi_note[channel] = 0xFF; // TODO: Check this
				}
				self.state.note_on_1[channel] =
					self.state.note_on_2[channel];
				self.state.note_on_2[channel] = false;
			}
		} else if self.state.fnum_2[channel] > 0
			&& self.state.fnum_2[channel]
				!= self.state.fnum_1[channel]
		{
			let dn_ret = do_note_on(
				self.state.note_1[channel],
				self.state.note_2[channel],
				midi_channel,
				&mut self.state.midi_note[channel],
				&mut self.state.midi_wheel[channel],
				Some(if self.state.note_on_2[channel] {
					0x0
				} else {
					0xFF
				}),
				None,
				self.config,
				midi,
			);
			self.state.note_on_1[channel] =
				self.state.note_on_2[channel];
			self.state.note_on_2[channel] = true;
			if dn_ret {
				self.state.note_delay[channel] = 0x0;
			}
		}
		if t6w28_sn76489 {
			channel += 0x04;
			midi_channel = midi_channel - MIDI_CHANNEL_SN76489_BASE;
			if self.state.note_2[channel] == 0xFF.into() {
				if self.state.note_on_2[channel] {
					if self.state.midi_note[channel] < 0xFF {
						do_note_on(
							self.state.note_1[channel],
							self.state.note_2[channel],
							midi_channel,
							&mut self.state.midi_note[channel],
							&mut self.state.midi_wheel
								[channel],
							None,
							None,
							self.config,
							midi,
						);
						//self.state.midi_note[channel] = 0xFF
					}
					self.state.note_on_1[channel] =
						self.state.note_on_2[channel];
					self.state.note_on_2[channel] = false;
				}
			} else if self.state.note_2[channel] < 0xFF.into()
				&& self.state.note_2[channel]
					!= self.state.note_1[channel]
			{
				let dn_ret = do_note_on(
					self.state.note_1[channel],
					self.state.note_2[channel],
					midi_channel,
					&mut self.state.midi_note[channel],
					&mut self.state.midi_wheel[channel],
					Some(if self.state.note_on_2[channel] {
						0x0
					} else {
						0xFF
					}),
					None,
					self.config,
					midi,
				);
				self.state.note_on_1[channel] =
					self.state.note_on_2[channel];
				self.state.note_on_2[channel] = true;
				if dn_ret {
					self.state.note_delay[channel] = 0x0
				}
			}
		}

		if (channel & 0x3) == 0x2 && self.state.clock_source[self.state.num as usize] == 0x3
		{
			self.command_handle(
				0xE0,
				(self.state.feedback[self.state.num as usize] << 2)
					| self.state.clock_source[self.state.num as usize],
				clock,
				t6w28_sn76489,
				midi,
			);
		}
	}

	fn process_tone_attenuation(
		&mut self,
		channel: u8,
		lsb: u8,
		midi_channel: u8,
		midi: &mut MIDIShim,
	) {
		self.state.attenuation_1[channel as usize] =
			self.state.attenuation_2[channel as usize];
		self.state.attenuation_2[channel as usize] = lsb & SN76489_ATTENUATION_MAX;

		if self.state.attenuation_2[channel as usize]
			!= self.state.attenuation_1[channel as usize]
		{
			//self.state.attenuation_2[channel as usize] = (LSB & SN76489_ATTENUATION_MAX) * 8.45 //(127 / 15)
			self.state.attenuation_2[channel as usize] = lsb & SN76489_ATTENUATION_MAX; // I like round values
									    //self.state.midi_volume = MIDI_VOLUME_MAX + 1 - 0x8 - self.state.attenuation_2[channel as usize] * 0x8
									    //if self.state.midi_volume > 0x7F{ self.state.midi_volume = 0x7F
			self.state.midi_volume = db_to_midi_vol(Self::vol_to_db(lsb & 0x0F));
			midi.event_write(Midi {
				channel: midi_channel.into(),
				message: Controller {
					controller: MIDI_VOLUME,
					value: self.state.midi_volume.into(),
				},
			});

			// write Note On/Off
			if self.config.sn76489_voldep_notes >= 1
				&& self.state.note_on_2[channel as usize]
			{
				if self.state.midi_volume == 0 {
					do_note_on(
						self.state.note_1[channel as usize],
						0xFF.into(),
						midi_channel,
						&mut self.state.midi_note[channel as usize],
						&mut self.state.midi_wheel[channel as usize],
						Some(0xFF),
						None,
						self.config,
						midi,
					);
					self.state.note_delay[channel as usize] = 44100;
				//} else if (SN76489_LastVol[channel as usize] = 0x0 & SN76489_NoteDelay[channel as usize] >= 10) | _
				//	(self.config.sn76489_voldep_notes >= 0x2 & SN76489_NoteDelay[channel as usize] >= 735 &
				//	SN76489_LastVol[channel as usize] + 20 < self.state.midi_volume) {
				} else if (self.state.attenuation_1[channel as usize] == 0x0F
					&& self.state.note_delay[channel as usize] >= 10)
					|| (self.config.sn76489_voldep_notes >= 0x2
						&& self.state.note_delay[channel as usize] >= 735
						&& self.state.attenuation_1[channel as usize] - 2
							> self.state.attenuation_2
								[channel as usize])
				{
					do_note_on(
						self.state.note_1[channel as usize],
						self.state.note_2[channel as usize],
						midi_channel,
						&mut self.state.midi_note[channel as usize],
						&mut self.state.midi_wheel[channel as usize],
						Some(0xFF),
						None,
						self.config,
						midi,
					);
					self.state.note_delay[channel as usize] = 0;
				}
			}
			//SN76489_LastVol[channel as usize] = self.state.midi_volume
		}
	}
	fn process_noise_attenuation(
		&mut self,
		channel: u8,
		lsb: u8,
		midi_channel: u8,
		midi: &mut MIDIShim,
	) {
		self.state.attenuation_1[channel as usize] =
			self.state.attenuation_2[channel as usize];
		self.state.attenuation_2[channel as usize] = lsb & SN76489_ATTENUATION_MAX;
		self.state.midi_volume = MIDI_VOLUME_MAX + 1
			- 8 - (self.state.attenuation_2[channel as usize]
			<< 3);
		if self.state.attenuation_2[channel as usize]
			!= self.state.attenuation_1[channel as usize]
			|| self.state.note_delay[channel as usize] >= 735
		{
			if self.state.midi_volume > 0 {
				// old Note-Height: 39
				if self.state.note_1[channel as usize] < 0xFF.into() {
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOff {
							key: (self.state.note_1[channel as usize]
								.round() as u8)
								.into(),
							vel: 0x00.into(),
						},
					});
				}
				if self.state.note_2[channel as usize] < 0xFF.into() {
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOn {
							key: (self.state.note_2[channel as usize]
								.round() as u8)
								.into(),
							vel: self.state.midi_volume.into(),
						},
					});
				}
				self.state.note_1[channel as usize] =
					self.state.note_2[channel as usize];
			} else if self.state.midi_volume == 0 {
				if self.state.attenuation_1[channel as usize] < 0x0F
					&& self.state.note_1[channel as usize] < 0xFF.into()
				{
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOff {
							key: (self.state.note_1[channel as usize]
								.round() as u8)
								.into(),
							vel: 0x00.into(),
						},
					});
					self.state.note_1[channel as usize] = 0xFF.into();
				}
			}
			self.state.note_delay[channel as usize] = 0x0;
		}
	}

	fn process_noise_channel(&mut self, lsb: u8, channel: u8, clock: u32) {
		self.state.feedback[self.state.num as usize] = (lsb & 0x4) / 0x4;
		//if !CBool(T6W28_SN76489 & SN76489_NUM > 0x0){
		self.state.clock_source[self.state.num as usize] = lsb & 0x3;
		//}

		//if self.state.clock_source[self.state.num] != SN76489_CLOCK_SOURCE_TONE_3{
		//	MIDIChannel = MIDI_CHANNEL_SN76489_BASE + 2
		//	let intNote: u16
		//	for intNote in 0..=127 {
		//		midi.event_write(Midi { channel: MIDIChannel, message: NoteOff { key: intNote, vel: 0x0 }});
		//	}
		//}

		// Noise-Frequency
		self.state.fnum_1[channel as usize] = self.state.fnum_2[channel as usize];
		self.state.fnum_2[channel as usize] =
			if self.state.clock_source[self.state.num as usize] == 0x3 {
				self.state.fnum_2[channel as usize - 1] << 1
			} else {
				1 << (5 + self.state.clock_source[self.state.num as usize] as u32)
			};

		if self.state.fnum_2[channel as usize] == 0 {
			self.state.fnum_2[channel as usize] = 1
		}

		self.state.hz_1[channel as usize] = self.state.hz_2[channel as usize];
		self.state.hz_2[channel as usize] =
			hz_sn76489(self.state.fnum_2[channel as usize], clock);

		//if T6W28_SN76489 & SN76489_NUM > 0x0{ return }

		self.state.note_2[channel as usize] = note(self.state.hz_2[channel as usize]);
		self.state.note_2[channel as usize] = self.state.note_2[channel as usize] / 1.5;
		//SN76489_NoteDelay[channel as usize] = 0x0
	}

	pub(crate) fn gg_stereo_handle(&mut self, register: u8, midi: &mut MIDIShim) {
		let mut pan_val: u8;
		let mut channel: u8;

		for cur_bit in 0x0..=0x3 {
			let channel_mask = 1 << cur_bit;

			pan_val = 0x0;
			if (register & (channel_mask << 4)) != 0 {
				pan_val = pan_val | 0x01; // Left Channel On
			}
			if (register & channel_mask) != 0 {
				pan_val = pan_val | 0x02; // Right Channel On
			}

			if self.state.gg_pan[self.state.num as usize][cur_bit as usize] != pan_val
				|| self.state.gg_pan[self.state.num as usize][0x4] == register
			{
				//if CurBit = 0x0{
				//	CH = CHN_DAC
				//} else {
				channel = MIDI_CHANNEL_SN76489_BASE + cur_bit;
				if self.state.num == 0x1 {
					channel = channel - MIDI_CHANNEL_SN76489_BASE
				}
				//}
				match pan_val {
					0x1 => midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_PAN,
							value: MIDI_PAN_LEFT,
						},
					}),
					0x2 => midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_PAN,
							value: MIDI_PAN_RIGHT,
						},
					}),
					0x3 => midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_PAN,
							value: MIDI_PAN_CENTER,
						},
					}),
					_ => (),
				};
			}
			self.state.gg_pan[self.state.num as usize][cur_bit as usize] = pan_val
		}

		self.state.gg_pan[self.state.num as usize][0x4] = register;
	}

	pub(crate) fn vol_to_db(tl: u8) -> f64 {
		if tl < 0x0F {
			-f64::from(tl) * 2.0
		} else {
			-400.0 // results in volume 0
		}
	}
}
