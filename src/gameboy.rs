use crate::ay8910::MIDI_CHANNEL_PSG_BASE;
use crate::config::Config;
use crate::midi_shim::db_to_midi_vol;
use crate::midi_shim::lin_to_db;
use crate::midi_shim::MIDIShim;
use crate::midi_shim::MIDI_PAN;
use crate::midi_shim::MIDI_PAN_CENTER;
use crate::midi_shim::MIDI_PAN_LEFT;
use crate::midi_shim::MIDI_PAN_RIGHT;
use crate::midi_shim::MIDI_VOLUME;
use crate::vgm2mid::do_note_on;
use crate::vgm2mid::note;
use midly::MidiMessage::Controller;
use midly::MidiMessage::NoteOff;
use midly::MidiMessage::ProgramChange;
use midly::TrackEventKind::Midi;

#[allow(dead_code)]
const NR10: u8 = 0x0;
const NR11: u8 = 0x1;
const NR12: u8 = 0x2;
const NR13: u8 = 0x3;
const NR14: u8 = 0x4;
const NR21: u8 = 0x6;
const NR22: u8 = 0x7;
const NR23: u8 = 0x8;
const NR24: u8 = 0x9;
const NR30: u8 = 0xA;
const NR31: u8 = 0xB;
const NR32: u8 = 0xC;
const NR33: u8 = 0xD;
const NR34: u8 = 0xE;
const NR41: u8 = 0x10;
const NR42: u8 = 0x11;
const NR43: u8 = 0x12;
const NR44: u8 = 0x13;
#[allow(dead_code)]
const NR50: u8 = 0x14;
const NR51: u8 = 0x15;
#[allow(dead_code)]
const NR52: u8 = 0x16;

fn hz_game_boy(fnum: u32) -> f64 {
	131072.0 / f64::from(2048 - fnum)
}

fn hz_game_boy_noise(poly_cntr: u8) -> f64 {
	let mut freq_div = f64::from(poly_cntr & 0x7); // Division Ratio of Freq
	let shft_frq: u16; // Shift Clock Freq (poly cntr)

	if freq_div == 0.0 {
		freq_div = 0.5
	}
	shft_frq = ((poly_cntr & 0xF0) / 0x10).into();
	524288.0 / freq_div / (2 ^ (shft_frq + 0x1)) as f64
}

pub(crate) struct GameBoyState {
	fnum_msb: [u8; 4],
	fnum_lsb: [u8; 4],
	fnum_1: [u32; 4],
	fnum_2: [u32; 4],
	hz_1: [f64; 4],
	hz_2: [f64; 4],
	note_1: [f64; 4],
	note_2: [f64; 4],
	envelope_1: [u8; 4],
	envelope_2: [u8; 4],
	duty_1: [u8; 4],
	duty_2: [u8; 4],
	midi_note: [u8; 4],
	midi_wheel: [u16; 4],
	note_on_1: [u8; 4],
	note_on_2: [u8; 4],
	midi_volume: u8,
	gb_pan: [u8; 5],
}

impl Default for GameBoyState {
	fn default() -> Self {
		GameBoyState {
			fnum_msb: [0; 4],
			fnum_lsb: [0; 4],
			fnum_1: [0; 4],
			fnum_2: [0; 4],
			hz_1: [0.0; 4],
			hz_2: [0.0; 4],
			note_1: [0xFF.into(); 4],
			note_2: [0xFF.into(); 4],
			envelope_1: [0; 4],
			envelope_2: [0xFF; 4],
			duty_1: [0; 4],
			duty_2: [0xFF; 4],
			midi_note: [0xFF; 4],
			midi_wheel: [0x8000; 4],
			note_on_1: [0; 4],
			note_on_2: [0; 4],
			midi_volume: 0,
			gb_pan: [0, 0, 0, 0, 0xFF],
		}
	}
}

pub(crate) fn gameboy_command_handle(
	register: u8,
	data: u8,
	state: &mut GameBoyState,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let channel: u8;
	let midi_channel: u8;
	let temp_byte: u8;

	/*if Variables_Clear_YM2612 = 1 {
		Erase FNum_1: Erase FNum_2: Erase Hz_1: Erase Hz_2: Erase Note_1: Erase Note_2
		Erase Envelope_1: Erase Envelope_2: Erase state.Duty1: Erase Duty_2: Erase NoteOn_1: Erase NoteOn_2
		Erase MIDINote: Erase MIDIWheel: Erase NoteOn_1: Erase NoteOn_2: MIDIVolume = 0
		for CH in 0x0..=0x3 {
			Envelope_2[channel as usize] = 0xFF
			Duty_2[channel as usize] = 0xFF
			state.note_1[channel as usize] = 0xFF
			state.note_2[channel as usize] = 0xFF
			state.note_on_2[channel as usize] = 0x0
			state.midi_note[channel as usize] = 0xFF
			state.midi_wheel[channel as usize] = 0x8000
		}
		for CH in 0x0..=0x3 {
			GB_Pan[channel as usize] = 0x0
		}
		GB_Pan(0x4) = 0xFF
		Variables_Clear_YM2612 = 0

		midi.event_write(MIDI_PROGRAM_CHANGE, MIDI_CHANNEL_PSG_BASE + 0x2, 0x50);
		midi.event_write(MIDI_PROGRAM_CHANGE, MIDI_CHANNEL_PSG_BASE + 0x3, 0x7F);
	}*/

	// Wave RAM
	if register >= 0x20 {
		return;
	}

	channel = register / 0x5;
	if channel < 0x3 {
		if config.sn76489_ch_disabled[channel as usize] {
			return;
		}
	} else if channel == 0x3 {
		if config.sn76489_noise_disabled {
			return;
		}
	}

	midi_channel = MIDI_CHANNEL_PSG_BASE + channel;
	match register {
		NR30 => {
			// Wave Channel - Note On
			state.note_on_1[channel as usize] = state.note_on_2[channel as usize];
			state.note_on_2[channel as usize] = data & 0x80;

			if state.note_on_1[channel as usize] != state.note_on_2[channel as usize] {
				if state.note_on_2[channel as usize] != 0 {
					do_note_on(
						state.note_2[channel as usize],
						state.note_2[channel as usize],
						midi_channel,
						&mut state.midi_note[channel as usize],
						&mut state.midi_wheel[channel as usize],
						Some(255),
						None,
						config,
						midi,
					);
				} else if state.midi_note[channel as usize] != 0xFF {
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOff {
							key: state.midi_note[channel as usize]
								.into(),
							vel: 0x00.into(),
						},
					});
					state.midi_note[channel as usize] = 0xFF
				}
			}
		},
		NR11 | NR21 | NR31 | NR41 => {
			// Sound Length, Wave pattern duty
			// how do I do this?
			if register == NR11 || register == NR21 {
				// Wave duties are: 12.5%, 25%, 50%, 75%
				state.duty_1[channel as usize] = state.duty_2[channel as usize];
				state.duty_2[channel as usize] = (data & 0xC0) / 0x40;

				if state.duty_1[channel as usize] != state.duty_2[channel as usize]
				{
					temp_byte = 0x4F + (!state.duty_2[channel as usize] & 0x3);
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: ProgramChange {
							program: temp_byte.into(),
						},
					});
				}
			}
		},
		NR12 | NR22 | NR32 | NR42 => {
			envelope(state, channel, register, data, midi, midi_channel);
		},
		NR13 | NR23 | NR33 | NR43 | NR14 | NR24 | NR34 | NR44 => {
			noise(register, state, channel, data, midi_channel, config, midi);
		},
		NR51 => gameboy_stereo(data, state, midi),
		_ => panic!(),
	}
}

fn envelope(
	state: &mut GameBoyState,
	channel: u8,
	register: u8,
	data: u8,
	midi: &mut MIDIShim,
	midi_channel: u8,
) {
	// Envelope
	state.envelope_1[channel as usize] = state.envelope_2[channel as usize];

	if register != NR32 {
		// output is 1 * envelope
		state.envelope_2[channel as usize] = (data & 0xF0) / 0x10;
		state.midi_volume = db_to_midi_vol(lin_to_db(
			f64::from(state.envelope_2[channel as usize]) / 0x0F as f64,
		));
	} else {
		// output is 0xF >> (envelope - 1)
		// >> 1 = 6 db
		state.envelope_2[channel as usize] = (data & 0x60) / 0x20;
		state.midi_volume =
			db_to_midi_vol(-f64::from(state.envelope_2[channel as usize]) * 6.0);
	}

	if state.envelope_1[channel as usize] != state.envelope_2[channel as usize] {
		midi.event_write(Midi {
			channel: midi_channel.into(),
			message: Controller {
				controller: MIDI_VOLUME,
				value: state.midi_volume.into(),
			},
		});
	}
}

fn noise(
	register: u8,
	state: &mut GameBoyState,
	channel: u8,
	data: u8,
	midi_channel: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	// Frequency Low, High
	if (register % 0x5) == 0x3 {
		state.fnum_lsb[channel as usize] = data;
	//Exit Sub
	} else if (register % 0x5) == 0x4 {
		state.fnum_msb[channel as usize] = data;
	}
	state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
	state.fnum_2[channel as usize] = ((state.fnum_msb[channel as usize] as u32 & 0x7) << 8)
		| (state.fnum_lsb[channel as usize] * 0x1) as u32;

	state.hz_1[channel as usize] = state.hz_2[channel as usize];
	if channel <= 0x1 {
		state.hz_2[channel as usize] = hz_game_boy(state.fnum_2[channel as usize]);
	} else if channel == 0x2 {
		state.hz_2[channel as usize] = hz_game_boy(state.fnum_2[channel as usize]) / 2.0;
	} else if channel == 0x3 {
		state.hz_2[channel as usize] =
			hz_game_boy_noise(state.fnum_lsb[channel as usize]) / 32.0;
	}

	state.note_1[channel as usize] = state.note_2[channel as usize];
	let mut temp_note = note(state.hz_2[channel as usize]);
	if temp_note >= 0x80.into() {
		//TempNote = 0x7F - state.fnum_2[channel as usize]
		temp_note = 0x7F.into();
	}
	state.note_2[channel as usize] = temp_note;

	if (register % 0x5) == 0x4 {
		if channel == 0x2 {
			state.note_on_1[channel as usize] = state.note_on_2[channel as usize]
		} else {
			// force NoteOn for all channels but the Wave channel
			state.note_on_1[channel as usize] = 0x0
		}
		state.note_on_2[channel as usize] =
			state.note_on_1[channel as usize] | (data & 0x80);

		if state.note_on_1[channel as usize] != state.note_on_2[channel as usize]
			&& state.note_on_2[channel as usize] != 0
		{
			do_note_on(
				state.note_1[channel as usize],
				state.note_2[channel as usize],
				midi_channel,
				&mut state.midi_note[channel as usize],
				&mut state.midi_wheel[channel as usize],
				Some(255),
				None,
				config,
				midi,
			);
		} else if state.note_on_2[channel as usize] != 0
			&& state.note_1[channel as usize] != state.note_2[channel as usize]
		{
			do_note_on(
				state.note_1[channel as usize],
				state.note_2[channel as usize],
				midi_channel,
				&mut state.midi_note[channel as usize],
				&mut state.midi_wheel[channel as usize],
				None,
				None,
				config,
				midi,
			);
		}
	} else if state.note_1[channel as usize] != state.note_2[channel as usize] {
		do_note_on(
			state.note_1[channel as usize],
			state.note_2[channel as usize],
			midi_channel,
			&mut state.midi_note[channel as usize],
			&mut state.midi_wheel[channel as usize],
			None,
			None,
			config,
			midi,
		);
	}
}

fn gameboy_stereo(data: u8, state: &mut GameBoyState, midi: &mut MIDIShim) {
	let mut pan_value = 0x00;

	if state.gb_pan[0x4] == data {
		return;
	}

	for current_bit in 0x0..=0x3 {
		let channel_mask = 2 ^ current_bit; // replaces "1 << CurBit"

		if data & (channel_mask * 0x10) != 0 {
			pan_value = pan_value | 0x1 // Left Channel On
		}
		if data & channel_mask != 0 {
			pan_value = pan_value | 0x2 // Right Channel On
		}

		if state.gb_pan[current_bit as usize] != pan_value || state.gb_pan[0x4] == data {
			let channel = MIDI_CHANNEL_PSG_BASE + current_bit;
			match pan_value {
				0x1 => midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_LEFT,
					},
				}),
				0x2 => midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_RIGHT,
					},
				}),
				0x3 => midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_CENTER,
					},
				}),
				_ => panic!(),
			};
		}
		state.gb_pan[current_bit as usize] = pan_value;
	}

	state.gb_pan[0x4] = data;
}
