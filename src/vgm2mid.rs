#[allow(unused_imports)]
use crate::midi_shim::{
	MIDIShim, MIDI_PAN, MIDI_PAN_CENTER, MIDI_PAN_LEFT, MIDI_PAN_RIGHT, MIDI_PATCH_GUNSHOT,
	MIDI_PATCH_LEAD_1_SQUARE, MIDI_PITCHWHEEL_CENTER, MIDI_PITCHWHEEL_MAX, MIDI_PITCHWHEEL_MIN,
	MIDI_RPN_LSB, MIDI_RPN_MSB, MIDI_VOLUME, RPN_PITCH_BEND_RANGE_L, RPN_PITCH_BEND_RANGE_M,
};
use crate::Config;
use midly::num::{u14, u7};
use midly::{
	MetaMessage::Tempo,
	MidiMessage::{Controller, NoteOff, NoteOn, PitchBend, ProgramChange},
	TrackEventKind::{Meta, Midi},
};

pub(crate) const CHN_DAC: u8 = 9;

#[allow(dead_code)]
pub(crate) const MIDI_NOTE_STEPS: u16 = 12;
//const PITCHWHEEL_SENSITIVITY = 64 //+/- n Semitones
//const PITCHWHEEL_STEPS_REAL = 8192 / PITCHWHEEL_SENSITIVITY
//const PITCHWHEEL_STEPS_DEFAULT = 4096 //16384 / 4
pub(crate) const PITCHWHEEL_SENSITIVITY_DEFAULT: u7 = u7::new(0x02); //+/- n Semitones
pub(crate) const PITCHWHEEL_STEPS_DEFAULT: u14 = u14::new(4096); //16384 / 4

//const MIDI_NOTE_CURVE = 2 ^ (1 / MIDI_NOTE_STEPS)
//const PITCHWHEEL_CURVE = MIDI_NOTE_CURVE ^ (1 / PITCHWHEEL_STEPS)

// Timing
//FIXME: Ensure that floating point numbers are the correct representation.
#[allow(dead_code)]
pub(crate) const OSC1: f64 = 53693100.0;
#[allow(dead_code)]
pub(crate) const FM_YM2413_SN76489: f64 = OSC1 / 15.0;
#[allow(dead_code)]
pub(crate) const FSAM_YM2413_SN76489: f64 = FM_YM2413_SN76489 / 72.0;
#[allow(dead_code)]
pub(crate) const FM_YM2612: f64 = OSC1 / 14.0;
#[allow(dead_code)]
pub(crate) const FSAM_YM2612: f64 = FM_YM2612 / 72.0;

pub(crate) const OPL_TYPE_YM3526: u8 = 0x1;
pub(crate) const OPL_TYPE_YM3812: u8 = 0x2;
pub(crate) const OPL_TYPE_YMF262: u8 = 0x3;
pub(crate) const OPL_TYPE_Y8950: u8 = 0x8;
pub(crate) const OPN_TYPE_YM2203: u8 = 0x1;
pub(crate) const OPN_TYPE_YM2608: u8 = 0x2;
pub(crate) const OPN_TYPE_YM2610: u8 = 0x3;
pub(crate) const OPN_TYPE_YM2612: u8 = 0x4;

//FIXME:
pub(crate) fn mid_data_init(
	config: &Config,
	midi: &mut MIDIShim,
	psg_on: bool,
	t6w28_sn76489: bool,
) {
	let mut tempo_val = 500000.0;

	//midi.event_write(MIDI_META_EVENT, META_TEMPO, 500000);
	if config.tempo_mod {
		//this was originally a comparison of TEMPO_MOD and 0x0 or 0x1.
		tempo_val *= f64::from(config.tempo_mult) / f64::from(config.tempo_div)
	} else {
		tempo_val *= 120.0 / f64::from(config.tempo_bpm)
	}
	midi.event_write(Meta(Tempo((tempo_val.round() as u32).into())));

	// I think there's no need to init the instruments for the FM Channels.
	// Most things are done before playing the first note.

	// Pan (Stereo) Settings for FM CHs 1-9 (Ch 3 Special)
	for channel in 0..=8 {
		midi.event_write(Midi {
			channel: channel.into(),
			message: Controller {
				controller: MIDI_PAN,
				value: MIDI_PAN_CENTER,
			},
		});
	}

	// Program Changes for FM CHs 1-9 (Ch 3 Special)
	//for CH in 0..=8 {
	//	midi.event_write(MIDI_PROGRAM_CHANGE, CH, MIDI_PATCH_Lead_8_Bass_Lead);
	//Next

	// Initial Volume Levels for FM CHs 1-9 (Ch 3 Special)
	//for CH in 0..=8 {
	//	midi.event_write(Midi { channel: CH, message: Controller { controller: MIDI_VOLUME, value: 95 }});
	//Next

	// Change Pitch Wheel Sensitivity for All CHs  (Ch 3 Special)
	if config.pitchwheel_sensitivity != PITCHWHEEL_SENSITIVITY_DEFAULT {
		for channel in 0x0..=0xF {
			if channel == 9 {
				// Setting the PB Range for Ch 9 (Drum-Channel) is unnecessary
				continue;
			}
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_RPN_MSB,
					value: RPN_PITCH_BEND_RANGE_M,
				},
			});
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_RPN_LSB,
					value: RPN_PITCH_BEND_RANGE_L,
				},
			});
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_RPN_MSB,
					value: config.pitchwheel_sensitivity.into(),
				},
			});
		}
	}

	// Settings for Drum Channel
	midi.event_write(Midi {
		channel: CHN_DAC.into(),
		message: Controller {
			controller: MIDI_PAN,
			value: MIDI_PAN_CENTER,
		},
	});
	midi.event_write(Midi {
		channel: CHN_DAC.into(),
		message: ProgramChange {
			program: 0x00.into(),
		},
	});
	midi.event_write(Midi {
		channel: CHN_DAC.into(),
		message: Controller {
			controller: MIDI_VOLUME,
			value: 0x7F.into(),
		},
	});

	if psg_on {
		// Pan (Stereo) Settings for SN76489 CHs 1-3 and Noise CH
		for channel in 10..=13 {
			if !t6w28_sn76489 {
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_CENTER,
					},
				});
			} else {
				midi.event_write(Midi {
					channel: (channel - 10).into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_RIGHT,
					},
				});
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: MIDI_PAN_LEFT,
					},
				});
			}
		}

		// Program Changes for SN76489 CHs 1-3 and Noise CH
		for channel in 10..=12 {
			if t6w28_sn76489 {
				midi.event_write(Midi {
					channel: (channel - 10).into(),
					message: ProgramChange {
						program: MIDI_PATCH_LEAD_1_SQUARE,
					},
				});
			}
			midi.event_write(Midi {
				channel: channel.into(),
				message: ProgramChange {
					program: MIDI_PATCH_LEAD_1_SQUARE,
				},
			});
		}
		if t6w28_sn76489 {
			midi.event_write(Midi {
				channel: 0x03.into(),
				message: ProgramChange {
					program: MIDI_PATCH_GUNSHOT,
				},
			});
		}
		midi.event_write(Midi {
			channel: 0x0D.into(),
			message: ProgramChange {
				program: MIDI_PATCH_GUNSHOT,
			},
		});

		// Initial Volume Levels for SN76489 CHs 1-3 and Noise CH
		for channel in 10..=13 {
			if t6w28_sn76489 {
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_VOLUME,
						value: 0x7F.into(),
					},
				});
			}
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_VOLUME,
					value: 0x7F.into(),
				},
			});
		}
	}
}

pub(crate) fn do_note_on(
	note_1: f64,
	note_2: f64,
	midi_channel: u8,
	midi_note: &mut u8,
	midi_wheel: &mut u16,
	opt_note_type: Option<u8>,
	opt_note_volume: Option<u8>,
	config: &Config,
	midi: &mut MIDIShim,
) -> bool {
	if note_1 < 0.0 || note_2 < 0.0 {
		panic!("Notes below 0")
	}
	let note_volume = opt_note_volume.unwrap_or(0x7F);
	let note_type = opt_note_type.unwrap_or(0);

	// the returned value ist used to detect written notes
	// (and avoid notes of Length 0)
	if note_type == 255 || note_1 == 0xFF as f64 || note_2 == 0xFF as f64 {
		return do_note_internal(
			note_2,
			midi_channel,
			midi_note,
			midi_wheel,
			note_volume,
			config,
			midi,
		);
	}
	//FIXME: wtf does this even mean?!
	let mut dbl_test_value =
		f64::from(*midi_wheel) + ((note_2 - note_1) * config.pitchwheel_steps as f64);

	//if Abs(Note_2 - Note_1) >= 0.5 {
	//	dblTestValue = -1
	//}
	if dbl_test_value >= MIDI_PITCHWHEEL_MIN.as_int() as f64
		&& dbl_test_value <= (MIDI_PITCHWHEEL_MAX.as_int() + 1) as f64
	{
		if dbl_test_value == MIDI_PITCHWHEEL_MIN.as_int() as f64 {
			dbl_test_value = MIDI_PITCHWHEEL_MAX.as_int() as f64;
		}
		*midi_wheel = dbl_test_value.round() as u16; //FIXME: This doesn't seem sane
		midi.event_write(Midi {
			channel: midi_channel.into(),
			message: PitchBend {
				bend: midly::PitchBend((*midi_wheel).into()),
			},
		});
	} else {
		return do_note_internal(
			note_2,
			midi_channel,
			midi_note,
			midi_wheel,
			note_volume,
			config,
			midi,
		);
	}

	false // only PitchBend written
}

fn do_note_internal(
	mut note_2: f64,
	midi_channel: u8,
	midi_note: &mut u8,
	midi_wheel: &mut u16,
	note_volume: u8,
	config: &Config,
	midi: &mut MIDIShim,
) -> bool {
	if note_2 >= f64::from(0xFF) {
		if *midi_note < 0xFF {
			midi.event_write(Midi {
				channel: midi_channel.into(),
				message: NoteOff {
					key: (*midi_note).into(),
					vel: 0x00.into(),
				},
			});
			*midi_note = note_2.round() as u8;
		}
		return true;
	}

	let mut dbl_pitch_wheel: f64 = note_2 - note_2.trunc();
	let new_wheel: u16;
	note_2 = note_2.trunc().abs();
	if dbl_pitch_wheel >= 0.5 {
		note_2 = note_2 + 1.0;
		dbl_pitch_wheel -= -1.0;
	}
	if *midi_note < 0xFF {
		midi.event_write(Midi {
			channel: midi_channel.into(),
			message: NoteOff {
				key: (*midi_note).into(),
				vel: 0x00.into(),
			},
		});
	}
	new_wheel = MIDI_PITCHWHEEL_CENTER.as_int()
		+ (dbl_pitch_wheel * f64::from(config.pitchwheel_steps)) as u16;
	if new_wheel != *midi_wheel {
		*midi_wheel = new_wheel;
		if !(midi_channel == CHN_DAC && dbl_pitch_wheel.abs() < 0.1) {
			midi.event_write(Midi {
				channel: midi_channel.into(),
				message: PitchBend {
					bend: midly::PitchBend((*midi_wheel).into()),
				},
			});
		}
	}
	*midi_note = note_2.round() as u8;
	midi.event_write(Midi {
		channel: midi_channel.into(),
		message: NoteOn {
			key: (*midi_note).into(),
			vel: note_volume.into(),
		},
	});

	true // wrote new Note
}

#[test]
fn test_note() {
	assert!((note(110.0) - 45.0).abs() < 0.01);
	assert!((note(220.0) - 57.0).abs() < 0.01);
	assert!((note(261.63) - 60.0).abs() < 0.01);
	assert!((note(440.0) - 69.0).abs() < 0.01);
	assert!((note(523.25) - 72.0).abs() < 0.01);
	assert!((note(880.0) - 81.0).abs() < 0.01);
	assert!((note(1760.0) - 93.0).abs() < 0.01);
}

//FIXME: smells
pub(crate) fn note(freq: f64) -> f64 {
	if freq == 0.into() {
		0xFF.into()
	} else {
		f64::log(freq / 440.0, f64::powf(2.0, 1.0 / 12.0)) + 69.0
	}
}
