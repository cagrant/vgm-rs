use crate::config::Config;
use crate::midi_shim::{
	MIDIShim, MIDI_DATA_ENTRY_MSB, MIDI_MODULATOR_WHEEL, MIDI_NRPN_LSB, MIDI_NRPN_MSB,
	MIDI_PAN, MIDI_VOLUME, NRPN_DRUM_PAN, NRPN_DRUM_PITCH_COARSE,
};
use crate::strict;
use crate::vgm2mid::{do_note_on, CHN_DAC};
use crate::ym3812::YM3812;
use anyhow::{anyhow, Result};
use midly::MidiMessage::{Controller, ProgramChange};
use midly::TrackEventKind::Midi;
use tock_registers::registers::ReadWrite;
use tock_registers::{register_bitfields, register_structs};

#[allow(dead_code)]
const YM278_REG_LSI_TEST: u8 = 0x00;
#[allow(dead_code)]
const YM278_REG_MEMORY_ACCESS_MODE: u8 = 0x02;
#[allow(dead_code)]
const YM278_REG_MEMORY_TYPE: u8 = 0x02;
#[allow(dead_code)]
const YM278_REG_WAVE_TABLE_HEADER: u8 = 0x02;

register_structs! {
	YMF278Registers {
		(0x00 => lsi_test: [ReadWrite<u8>; 0x02]),
		(0x02 => header: ReadWrite<u8, >),
		(0x03 => memory_access1: ReadWrite<u8, MemoryAccess1::Register>),
		(0x04 => memory_access2: ReadWrite<u8>),
		(0x05 => memory_access3: ReadWrite<u8>),
		(0x06 => memory_data: ReadWrite<u8>),
		(0x07 => reserved: ReadWrite<u8>),
		(0x08 => wave_table_number: [ReadWrite<u8>; 0x18]),
		(0x20 => f_number: [ReadWrite<u8, FNumber::Register>; 0x18]),
		(0x38 => octave: [ReadWrite<u8, Octave::Register>; 0x18]),
		(0x50 => total_level: [ReadWrite<u8, TotalLevel::Register>; 0x18]),
		(0x68 => key_on: [ReadWrite<u8, KeyOn::Register>; 0x18]),
		(0x80 => lfo: [ReadWrite<u8, LFO::Register>; 0x18]),
		(0x98 => ar: [ReadWrite<u8, AR::Register>; 0x18]),
		(0xB0 => dl: [ReadWrite<u8, DL::Register>; 0x18]),
		(0xC8 => rate_correction: [ReadWrite<u8, RateCorrection::Register>; 0x18]),
		(0xE0 => am: [ReadWrite<u8, AM::Register>; 0x18]),
		(0xF8 => mixing_control_fm: ReadWrite<u8, MixingControlFM::Register>),
		(0xF9 => mixing_control_pcm: ReadWrite<u8, MixingControlPCM::Register>),
		(0xFA => @END),
	}
}

register_bitfields![u8,
Status [
	IRQ 7,
	FT1 6,
	FT2 5,
	LD 1,
	BUSY 0,
],
MemoryAccess1 [
	RESERVED OFFSET(7) NUMBITS(2) [],
	MEMORYACCESS OFFSET(0) NUMBITS(6) [],
],
FNumber [
	F_NUMBER OFFSET(1) NUMBITS(7) [],
	WAVETABLE OFFSET(0) NUMBITS(1) [],
],
Octave [
  OCTAVE OFFSET(4) NUMBITS(4) [],
  PSEUDO_REVERB OFFSET(3) NUMBITS (1) [],
  F_NUMBER OFFSET(0) NUMBITS(3) [],
],
TotalLevel [
  TOTAL_LEVEL OFFSET(1) NUMBITS(7) [],
  LEVEL_DIRECT OFFSET(0) NUMBITS(1) [],
],
KeyOn [
  KEY_ON OFFSET(7) NUMBITS(1) [],
  DAMP OFFSET(6) NUMBITS(1) [],
  LFO_RST OFFSET(5) NUMBITS(1) [],
  CH OFFSET(4) NUMBITS(1) [],
  PANPOT OFFSET(0) NUMBITS(4) [],
],
LFO [
	RESERVED OFFSET(6) NUMBITS(2) [],
	LFO OFFSET(3) NUMBITS(3) [],
	VIB OFFSET(0) NUMBITS(3) [],
],
AR [
	AR OFFSET(3) NUMBITS(4) [],
	D1R OFFSET(0) NUMBITS(4) [],
],
DL [
	DL OFFSET(3) NUMBITS(4) [],
	D2R OFFSET(0) NUMBITS(4) [],
],
RateCorrection [
	Rate_Correction OFFSET(3) NUMBITS(4) [],
	RR OFFSET(3) NUMBITS(4) [],
],
AM [
	RESERVED OFFSET(3) NUMBITS(5) [],
	AM OFFSET(0) NUMBITS(3) [],
],
MixingControlFM [
	FM_R OFFSET(3) NUMBITS(3) [],
	FM_L OFFSET(0) NUMBITS(3) [],
],
MixingControlPCM [
	PCM_R OFFSET(3) NUMBITS(3) [],
	PCM_L OFFSET(0) NUMBITS(3) [],
],
];

struct Opl4Tone {
	instrument: u8,
	pitch: u16,
	#[allow(dead_code)]
	key_scl: u8,
}

const XG_MODE: bool = false;

static OPL4_TONES: Vec<Opl4Tone> = Vec::new();

fn note_ymf278(tone: u16, fnum: u32, oct: i16) -> f64 {
	if oct == -8 && fnum == 0 {
		return 0xFF.into();
	}

	let mut pitch: f64;
	let octave: i16;
	let note: f64;

	// Note2FNum from Linux// OPL4 Driver
	//pitch = (note - 60) * voice->sound->key_scaling / 100 + 60;
	//pitch = pitch << 7;	-- 1 Halftone = 128 pitch --
	//pitch += voice->sound->pitch_offset;
	//octave = pitch / 0x600 - 8;
	//fnum = pitch2fnum(pitch % 0x600);

	pitch = (1.0 + fnum as f64 / 1024.0).log2();
	octave = oct + 8;
	//if OPL4Tones(Tone).Pitch = 0x1000 { Stop
	pitch = pitch - OPL4_TONES[tone as usize].pitch as f64 / 0x600 as f64;
	// Formula from YMF278B Manual:			 1024 + F_NUMBER
	// F(c) = 1200 x (Octave - 1) + 1200 x log2 ---------------
	// 1 octave = 1200c							   1024
	note = (octave as f64 * 12.0) + pitch * 12.0;
	//if OPL4Tones(Tone).KeyScl > 0 & OPL4Tones(Tone).KeyScl != 100 {
	//	Note = (Note - 60) * 100 / OPL4Tones(Tone).KeyScl + 60
	//}

	note
}

fn channel_select(snum: u8, tone_ins: u8) -> (u8, bool) {
	let mut select_channel: u8;
	let drum_mode: bool;

	//FIXME: more false constants
	if XG_MODE {
		select_channel = snum & 0xF;
		drum_mode = false;
	} else {
		if tone_ins & 0x80 == 0 {
			select_channel = snum & 0xF;
			if select_channel == CHN_DAC {
				select_channel = 0xF;
			}
			//if SNum <= 0x4 { SelChn = 0x8
			drum_mode = false;
		} else {
			select_channel = CHN_DAC;
			drum_mode = true;
		}
	}
	(select_channel, drum_mode)
}

pub(crate) struct YMF278State {
	fnum_msb: [u8; 0x18],
	fnum_lsb: [u8; 0x18],
	tone_wave: [u16; 0x18],
	tone_ins: [u8; 0x18],
	oct: [i16; 0x18],
	fnum_o: [u32; 0x18],
	fnum_n: [u32; 0x18],
	note_o: [f64; 0x18],
	note_n: [f64; 0x18],
	note_on_o: [u8; 0x18],
	note_on_n: [u8; 0x18],
	midi_note: [u8; 0x18],
	midi_wheel: [u16; 0x18],
	slot_volume: [u8; 0x18],
	slot_pan: [u8; 0x18],
	damper_on_o: [u8; 0x18],
	damper_on_n: [u8; 0x18],
	vib: [u8; 0x18],
	midi_volume: [u8; 0x10],
	midi_pan: [u8; 0x10],
	midi_ins: [u8; 0x10],
	drum_pan: [[u8; 0x80]; 0x10],
	drum_pitch: [[u8; 0x80]; 0x10],
	drum_nrpn_m: [u8; 0x10],
	drum_nrpn_l: [u8; 0x10],
	opl4_tones: Vec<Opl4Tone>,
}

pub(crate) struct YMF278 {
	pub state: YMF278State,
	config: Config,
}

impl YMF278 {
	pub(crate) fn new(config: &Config, opt_state: Option<YMF278State>) -> YMF278 {
		YMF278 {
			state: opt_state.unwrap_or(Default::default()),
			config: config.to_owned(),
		}
	}
	pub(crate) fn command_handle(
		&mut self,
		port: u8,
		register: u8,
		data: u8,
		ym3812: &mut YM3812,
		fsam_3812: f64,
		midi: &mut MIDIShim,
	) -> Result<()> {
		if port < 0x2 {
			ym3812.command_handle(
				(port as u16) << 8 | register as u16,
				data,
				fsam_3812,
				midi,
			)?;
			if port == 0x1 && register == 0x5 {
				*&mut self.state = Default::default();
			}
			return Ok(());
		}

		if register >= 0x8 && register <= 0xF7 {
			let slot_number = (register - 0x8) % 24;
			let (channel, drum_mode) = channel_select(
				slot_number,
				self.state.tone_ins[slot_number as usize],
			);

			//FIXME: this is dumb, make this more sensible.
			match (register - 0x8) / 24 {
				0x0 => {
					wave_table_number(
						&mut self.state,
						slot_number,
						data,
						channel,
						&self.config,
						midi,
					);
				},
				0x1 => {
					f_number(&mut self.state, slot_number, data);
				},
				0x2 => {
					octave(
						&mut self.state,
						slot_number,
						data,
						channel,
						&self.config,
						midi,
					);
				},
				0x3 => {
					total_level(
						&mut self.state,
						slot_number,
						data,
						drum_mode,
						channel,
						midi,
					);
				},
				0x4 => {
					key_on(
						data,
						&mut self.state,
						slot_number,
						drum_mode,
						channel,
						&self.config,
						midi,
					);
				},
				0x5 => {
					lfo(
						&mut self.state,
						slot_number,
						data,
						drum_mode,
						channel,
						midi,
					);
				},
				0x6 => {
					ar(
						&mut self.state,
						slot_number,
						data,
						channel,
						&self.config,
						midi,
					);
				},
				0x7 => (), // Reg 0xB0 - 0xC7
				//slot.DL = dl_tab(Int(Data / 0x10))
				//slot.D2R = Data & 0xF
				0x8 => (), // Reg 0xC8 - 0xDF
				//slot.RC = Int(Data / 0x10)
				//slot.RR = Data & 0xF
				0x9 => (), // Reg 0xE0 - 0xF7
				//slot.AM = Data & 0x7
				_ => panic!(),
			}
		} else {
			// All non-slot registers
			match register {
				0x0 => (), // TEST
				0x1 => (),
				0x2 => (),
				//wavetblhdr = (Data >> 2) & 0x7
				//memmode = Data & 1
				0x3 => (),
				//memadr = (memadr & 0xFFFF&) | (Data * 0x10000)
				0x4 => (),
				//memadr = (memadr & 0xFF00FF) | (Data * 0x100&)
				0x5 => (),
				//memadr = (memadr & 0xFFFF00) | Data
				0x6 => (), // memory data
				//Call writeMem(memadr, Data)
				//memadr = (memadr + 1) & 0xFFFFFF
				0xF8 => (),
				//fm_l = Data & 0x7
				//fm_r = (Data >> 3) & 0x7
				0xF9 => (),
				//pcm_l = Data & 0x7
				//pcm_r = (Data >> 3) & 0x7
				_ => (),
			}
		}

		//regs [Register] = Data

		Ok(())
	}

	pub(crate) fn load_opl4_instrument_set(&mut self) -> Result<()> {
		const INSSET_SIG: &[u8] = b"INSSET";
		const COUNT_PTR: usize = INSSET_SIG.len(); //This should resolve to the subsequent byte.

		//NOTE: Consider adding this as a static resource?
		//There is at most 1 KiB of data. Even on an embedded device it would be reasonable to
		//just statically allocate this.
		let bytes = std::fs::read("yrw801.ins").map_err(|err| anyhow!(err))?;

		if bytes.len() <= INSSET_SIG.len() {
			strict!("OPL4 Instrument file is too short!");
		}

		if &bytes[0..INSSET_SIG.len()] != INSSET_SIG {
			strict!("Invalid OPL4 instrument set");
		}

		let tone_count = bytes[COUNT_PTR];

		for tone in 0..tone_count {
			let instrument = bytes[COUNT_PTR + 1 + (tone * 4) as usize];
			let pitch = u16::from_le_bytes([
				bytes[COUNT_PTR + 1 + (1 + tone * 4) as usize],
				bytes[COUNT_PTR + 1 + (2 + tone * 4) as usize],
			]);
			let key_scl = bytes[COUNT_PTR + 1 + (3 + tone * 4) as usize];
			self.state.opl4_tones.push(Opl4Tone {
				instrument,
				pitch,
				key_scl,
			});
		}
		Ok(())
	}
}

impl Default for YMF278State {
	fn default() -> Self {
		YMF278State {
			fnum_msb: [0; 0x18],
			fnum_lsb: [0; 0x18],
			tone_wave: [0; 0x18],
			tone_ins: [0; 0x18],
			oct: [0; 0x18],
			fnum_o: [0; 0x18],
			fnum_n: [0; 0x18],
			note_o: [0xFF.into(); 0x18],
			note_n: [0xFF.into(); 0x18],
			note_on_o: [0; 0x18],
			note_on_n: [0; 0x18],
			midi_note: [0; 0x18],
			midi_wheel: [0x8000; 0x18],
			slot_volume: [0; 0x18],
			slot_pan: [0; 0x18],
			damper_on_o: [0; 0x18],
			damper_on_n: [0; 0x18],
			vib: [0; 0x18],
			midi_volume: [0xFF; 0x10],
			midi_pan: [0xFF; 0x10],
			midi_ins: [0xFF; 0x10],
			drum_pan: [[0xFF; 0x80]; 0x10],
			drum_pitch: [[0xFF; 0x80]; 0x10],
			drum_nrpn_m: [0xFF; 0x10],
			drum_nrpn_l: [0xFF; 0x10],
			opl4_tones: Vec::new(),
		}
	}
}

fn ar(
	state: &mut YMF278State,
	slot_number: u8,
	data: u8,
	channel: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	// Reg 0x98 - 0xAF
	//slot.AR = Int(Data / 0x10)
	//slot.D1R = Data & 0xF
	if state.tone_ins[slot_number as usize] == 0x77 {
		// Reverse Cymbal
		let temp_byte = data / 0x10;
		if temp_byte >= 0xE {
			// -> Crash Cymbal
			if state.note_on_o[slot_number as usize] != 0 {
				do_note_on(
					state.note_o[slot_number as usize],
					0xFF.into(),
					channel,
					&mut state.midi_note[slot_number as usize],
					&mut state.midi_wheel[slot_number as usize],
					Some(0xFF),
					Some(temp_byte),
					config,
					midi,
				);
				state.note_on_o[slot_number as usize] = 0;
			}

			state.tone_wave[slot_number as usize] = 0x180;
			state.tone_ins[slot_number as usize] = OPL4_TONES
				[state.tone_wave[slot_number as usize] as usize]
				.instrument;
			let (channel, _drum_mode) =
				channel_select(slot_number, state.tone_ins[slot_number as usize]);
			if state.tone_ins[slot_number as usize] & 0x80 != 0 {
				let temp_byte = state.tone_ins[slot_number as usize] & 0x7F;
				let old_val =
					state.drum_pitch[channel as usize][temp_byte as usize];
				state.drum_pitch[channel as usize][temp_byte as usize] =
					state.note_n[slot_number as usize] as u8; //FIXME: same dumbass shit
				if state.drum_pitch[channel as usize][temp_byte as usize] != old_val
				{
					if state.drum_nrpn_m[channel as usize]
						!= NRPN_DRUM_PITCH_COARSE || state.drum_nrpn_l
						[channel as usize]
						!= temp_byte
					{
						state.drum_nrpn_m[channel as usize] =
							NRPN_DRUM_PITCH_COARSE.into();
						state.drum_nrpn_l[channel as usize] = temp_byte;
						midi.event_write(Midi {
							channel: channel.into(),
							message: Controller {
								controller: MIDI_NRPN_MSB,
								value: state.drum_nrpn_m
									[channel as usize]
									.into(),
							},
						});
						midi.event_write(Midi {
							channel: channel.into(),
							message: Controller {
								controller: MIDI_NRPN_LSB,
								value: state.drum_nrpn_l
									[channel as usize]
									.into(),
							},
						});
					}
					midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_DATA_ENTRY_MSB,
							value: state.drum_pitch[channel as usize]
								[temp_byte as usize]
								.into(),
						},
					});
				}

				state.note_n[slot_number as usize] =
					(state.tone_ins[slot_number as usize] & 0x7F).into();
			}
		}
	}
}

fn lfo(
	state: &mut YMF278State,
	slot_number: u8,
	data: u8,
	drum_mode: bool,
	channel: u8,
	midi: &mut MIDIShim,
) {
	// Reg 0x80 - 0x97
	let old_val = state.vib[slot_number as usize];
	state.vib[slot_number as usize] = (data & 0x7) * 0x10;
	if !drum_mode && state.vib[slot_number as usize] != old_val {
		midi.event_write(Midi {
			channel: channel.into(),
			message: Controller {
				controller: MIDI_MODULATOR_WHEEL,
				value: state.vib[slot_number as usize].into(),
			},
		});
	}
	//slot.set_lfo(Int(Data / 0x8) & 0x7)
}

fn key_on(
	data: u8,
	state: &mut YMF278State,
	slot_number: u8,
	drum_mode: bool,
	channel: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	// Reg 0x68 - 0x7F
	if data & 0x80 != 0 {
		let temp_byte = data & 0xF;
		if temp_byte < 0x8 {
			state.slot_pan[slot_number as usize] = 0x40 - temp_byte / 0x7 * 0x40;
		} else if temp_byte > 0x8 {
			state.slot_pan[slot_number as usize] =
				0x40 + (0x10 - temp_byte) / 0x7 * 0x40;
			if state.slot_pan[slot_number as usize] == 0x80 {
				state.slot_pan[slot_number as usize] = 0x7F
			}
		} else {
			state.slot_pan[slot_number as usize] = 0x40;
		}

		if !drum_mode {
			let old_val = state.midi_pan[channel as usize];
			state.midi_pan[channel as usize] = state.slot_pan[slot_number as usize];
			if state.midi_pan[channel as usize] != old_val {
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_PAN,
						value: state.midi_pan[channel as usize].into(),
					},
				});
			}
		} else {
			let temp_byte = state.tone_ins[slot_number as usize] & 0x7F;
			let old_val = state.drum_pan[channel as usize][temp_byte as usize];
			state.drum_pan[channel as usize][temp_byte as usize] =
				state.slot_pan[slot_number as usize];
			if state.drum_pan[channel as usize][temp_byte as usize] != old_val {
				if state.drum_nrpn_m[channel as usize] != NRPN_DRUM_PAN
					|| state.drum_nrpn_l[channel as usize] != temp_byte
				{
					state.drum_nrpn_m[channel as usize] = NRPN_DRUM_PAN.into();
					state.drum_nrpn_l[channel as usize] = temp_byte;
					midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_NRPN_MSB,
							value: state.drum_nrpn_m[channel as usize]
								.into(),
						},
					});
					midi.event_write(Midi {
						channel: channel.into(),
						message: Controller {
							controller: MIDI_NRPN_LSB,
							value: state.drum_nrpn_l[channel as usize]
								.into(),
						},
					});
				}
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_DATA_ENTRY_MSB,
						value: state.drum_pan[channel as usize]
							[temp_byte as usize]
							.into(),
					},
				});
			}
		}
	}

	if data & 0x20 != 0 {
		// LFO reset
	} else {
		// LFO activate
	}

	state.damper_on_n[slot_number as usize] = (data & 0x40) / 0x40;
	state.note_on_n[slot_number as usize] = (data & 0x80) / 0x80;

	if state.damper_on_n[slot_number as usize] != state.damper_on_o[slot_number as usize] {
		// Damping increases Decay rate
		//midi.event_write(MIDI_CONTROLLER_CHANGE, CH, MIDI_SOFT, DamperOn_N[SNum]) & 0x7F != 0;
		state.damper_on_o[slot_number as usize] = state.damper_on_n[slot_number as usize]
	}
	if state.note_on_n[slot_number as usize] != state.note_on_o[slot_number as usize] {
		let temp_byte = if !drum_mode {
			0x7F
		} else {
			state.slot_volume[slot_number as usize]
		};
		if state.note_on_n[slot_number as usize] != 0 {
			do_note_on(
				state.note_o[slot_number as usize],
				state.note_n[slot_number as usize],
				channel,
				&mut state.midi_note[slot_number as usize],
				&mut state.midi_wheel[slot_number as usize],
				Some(0xFF),
				Some(temp_byte),
				config,
				midi,
			);
		} else {
			do_note_on(
				state.note_o[slot_number as usize],
				0xFF.into(),
				channel,
				&mut state.midi_note[slot_number as usize],
				&mut state.midi_wheel[slot_number as usize],
				Some(0xFF),
				Some(temp_byte),
				config,
				midi,
			);
		}
		state.note_on_o[slot_number as usize] = state.note_on_n[slot_number as usize]
	}
}

fn total_level(
	state: &mut YMF278State,
	slot_number: u8,
	data: u8,
	drum_mode: bool,
	channel: u8,
	midi: &mut MIDIShim,
) {
	// Reg 0x50 - 0x67
	let old_val = state.slot_volume[slot_number as usize];
	state.slot_volume[slot_number as usize] = 0x7F - data / 0x2;
	if state.slot_volume[slot_number as usize] == 0x0
		&& state.note_on_n[slot_number as usize] == 0
	{
		state.slot_volume[slot_number as usize] = old_val;
	}
	//LD = Data & 0x1

	//if LD {
	//	// directly change volume
	//} else {
	//	// interpolate volume
	//}

	if !drum_mode {
		let old_val = state.midi_volume[channel as usize];
		state.midi_volume[channel as usize] = state.slot_volume[slot_number as usize];

		if state.midi_volume[channel as usize] != old_val {
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_VOLUME,
					value: state.midi_volume[channel as usize].into(),
				},
			});
		}
	}
}

fn octave(
	state: &mut YMF278State,
	slot_number: u8,
	data: u8,
	channel: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	// Reg 0x38 - 0x4F
	//slot.FN = (slot.FN & 0x07F) | ((Data & 0x07) << 7)
	state.fnum_msb[slot_number as usize] = data & 0x7;
	//slot.PRVB = ((data & 0x08) >> 3)
	//slot.OCT =  ((data & 0xF0) >> 4)
	state.oct[slot_number as usize] = ((data & 0xF0) / 0x10).into();
	//int oct = slot.OCT;
	if state.oct[slot_number as usize] & 0x8 != 0 {
		state.oct[slot_number as usize] = state.oct[slot_number as usize] | -8;
	}

	state.fnum_o[slot_number as usize] = state.fnum_n[slot_number as usize];
	state.fnum_n[slot_number as usize] = ((state.fnum_msb[slot_number as usize] * 0x80)
		| state.fnum_lsb[slot_number as usize])
		.into();

	state.note_o[slot_number as usize] = state.note_n[slot_number as usize];

	state.note_n[slot_number as usize] = note_ymf278(
		state.tone_wave[slot_number as usize],
		state.fnum_n[slot_number as usize],
		state.oct[slot_number as usize],
	);
	if state.tone_ins[slot_number as usize] & 0x80 != 0 {
		let temp_byte = state.tone_ins[slot_number as usize] & 0x7F;
		let old_val = state.drum_pitch[channel as usize][temp_byte as usize];
		state.drum_pitch[channel as usize][temp_byte as usize] =
			state.note_n[slot_number as usize] as u8; //FIXME wtf is this shit.
		if state.drum_pitch[channel as usize][temp_byte as usize] != old_val {
			if state.drum_nrpn_m[channel as usize] != NRPN_DRUM_PITCH_COARSE
				|| state.drum_nrpn_l[channel as usize] != temp_byte
			{
				state.drum_nrpn_m[channel as usize] = NRPN_DRUM_PITCH_COARSE.into();
				state.drum_nrpn_l[channel as usize] = temp_byte;
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_NRPN_MSB,
						value: state.drum_nrpn_m[channel as usize].into(),
					},
				});
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: MIDI_NRPN_LSB,
						value: state.drum_nrpn_l[channel as usize].into(),
					},
				});
			}
			midi.event_write(Midi {
				channel: channel.into(),
				message: Controller {
					controller: MIDI_DATA_ENTRY_MSB,
					value: state.drum_pitch[channel as usize]
						[temp_byte as usize]
						.into(),
				},
			});
		}

		state.note_n[slot_number as usize] =
			(state.tone_ins[slot_number as usize] & 0x7F).into();
	}

	if state.note_on_n[slot_number as usize] != 0 {
		do_note_on(
			state.note_o[slot_number as usize],
			state.note_n[slot_number as usize],
			channel,
			&mut state.midi_note[slot_number as usize],
			&mut state.midi_wheel[slot_number as usize],
			None,
			None,
			config,
			midi,
		);
	}
}

fn f_number(state: &mut YMF278State, slot_number: u8, data: u8) {
	// Reg 0x20 - 0x37
	//slot.wave = (slot.wave & 0xFF) | ((data & 0x1) << 8);
	state.tone_wave[slot_number as usize] =
		(state.tone_wave[slot_number as usize] & 0xFF) | (data as u16 & 0x1) * 0x100;
	//slot.FN = (slot.FN & 0x380) | (data >> 1);
	state.fnum_lsb[slot_number as usize] = data / 0x2;
	// usually, FNum MSB is sent after FNum LSB
}

fn wave_table_number(
	state: &mut YMF278State,
	slot_number: u8,
	data: u8,
	channel: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	// Reg 0x8 - 0x1F
	//loadTime = time + LOAD_DELAY;
	//
	//slot.wave = (slot.wave & 0x100) | data;
	state.tone_wave[slot_number as usize] =
		(state.tone_wave[slot_number as usize] & 0x100) | data as u16;
	state.tone_ins[slot_number as usize] =
		OPL4_TONES[state.tone_wave[slot_number as usize] as usize].instrument;

	let old_val = channel;
	let (channel, _drum_mode) =
		channel_select(slot_number, state.tone_ins[slot_number as usize]);
	if channel != old_val & state.note_on_o[slot_number as usize] {
		do_note_on(
			state.note_o[slot_number as usize],
			0xFF.into(),
			channel,
			&mut state.midi_note[slot_number as usize],
			&mut state.midi_wheel[slot_number as usize],
			Some(0xFF),
			Some(0x00), //FIXME: This was temp_byte, though it was never assigned...
			config,
			midi,
		);
		state.note_on_o[slot_number as usize] = 0
	}

	let old_val = state.midi_ins[channel as usize];
	state.midi_ins[channel as usize] = if state.tone_ins[slot_number as usize] & 0x80 != 0 {
		0x80
	} else {
		state.tone_ins[slot_number as usize] & 0x7F
	};

	if state.midi_ins[channel as usize] != old_val {
		if XG_MODE {
			if (state.midi_ins[channel as usize] & 0x80) != (old_val & 0x80) {
				let temp_byte = if state.midi_ins[channel as usize] & 0x80 != 0 {
					0x7F
				} else {
					0x0
				};
				midi.event_write(Midi {
					channel: channel.into(),
					message: Controller {
						controller: 0x00.into(),
						value: temp_byte.into(),
					},
				});
			}
			if (state.midi_ins[channel as usize] & 0x80) & (old_val & 0x80) == 0 {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: (state.midi_ins[channel as usize] & 0x7F)
							.into(),
					},
				});
			}
		} else {
			if state.midi_ins[channel as usize] & 0x80 == 0 {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: (state.midi_ins[channel as usize] & 0x7F)
							.into(),
					},
				});
			}
		}
		//if CH < 8 & MIDIIns[channel as usize] = 0x77 { Stop
	}
	//int base = (slot.wave < 384 | !wavetblhdr) ?
	//		   (slot.wave * 12) :
	//		   (wavetblhdr * 0x80000 + ((slot.wave - 384) * 12));
	//byte buf[12];
	//for (int i = 0; i < 12; ++i) {
	//	buf[i] = readMem(base + i);
	//}
	//slot.bits = (buf[0] & 0xC0) >> 6;
	//slot.set_lfo((buf[7] >> 3) & 7);
	//slot.vib  = buf[7] & 7;
	//slot.AR   = buf[8] >> 4;
	//slot.D1R  = buf[8] & 0xF;
	//slot.DL   = dl_tab[buf[9] >> 4];
	//slot.D2R  = buf[9] & 0xF;
	//slot.RC   = buf[10] >> 4;
	//slot.RR   = buf[10] & 0xF;
	//slot.AM   = buf[11] & 7;
	//slot.startaddr = buf[2] | (buf[1] << 8) |
	//				 ((buf[0] & 0x3F) << 16);
	//slot.loopaddr = buf[4] + (buf[3] << 8);
	//slot.endaddr  = (((buf[6] + (buf[5] << 8)) ^ 0xFFFF) + 1);
	//if ((regs[Register + 4] & 0x080)) {
	//	 keyOnHelper(slot);
	//}
}
