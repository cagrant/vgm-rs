// OPL Module for YM3812, YM3526, Y8950 and YMF262, based on YM2413 (OPLL) Module

use crate::config::Config;
use crate::midi_shim::{
	db_to_midi_vol, MIDIShim, MIDI_PAN, MIDI_PAN_CENTER, MIDI_PAN_LEFT, MIDI_PAN_RIGHT,
	MIDI_VOLUME,
};
use crate::strict;
use crate::vgm2mid::{do_note_on, note, CHN_DAC, OPL_TYPE_YMF262};
use anyhow::Result;
use midly::MidiMessage::Controller;
use midly::MidiMessage::NoteOff;
use midly::MidiMessage::NoteOn;
use midly::MidiMessage::ProgramChange;
use midly::TrackEventKind::Midi;

// YM3812/YMF262 Register Constants
const YM3812_REG_TEST_WAVESEL_EN: u16 = 0x1;
const YM3812_REG_CSW_NOTESEL: u16 = 0x8;
const YM3812_REG_MOD_VIB_EG_KS_MULT: u16 = 0x20;
const YM3812_REG_MOD_VIB_EG_KS_MULT_END: u16 = YM3812_REG_MOD_VIB_EG_KS_MULT + 0x15;
const YM3812_REG_KSL_TL: u16 = 0x40;
const YM3812_REG_KSL_TL_END: u16 = YM3812_REG_KSL_TL + 0x17;
#[allow(dead_code)]
const YM3812_REG_AR_DR: u16 = 0x60;
#[allow(dead_code)]
const YM3812_REG_SL_RR: u16 = 0x80;
const YM3812_REG_FNUM_LSB: u16 = 0xA0;
const YM3812_REG_FNUM_LSB_END: u16 = YM3812_REG_FNUM_LSB + 0x8;
const YM3812_REG_KEY_BLOCK_FNUM_MSB: u16 = 0xB0;
const YM3812_REG_KEY_BLOCK_FNUM_MSB_END: u16 = YM3812_REG_KEY_BLOCK_FNUM_MSB + 0x8;
const YM3812_REG_RHYTHM: u16 = 0xBD;
const YM3812_FB_CONNECTION_PAN: u16 = 0xC0;
const YM3812_FB_CONNECTION_PAN_END: u16 = YM3812_FB_CONNECTION_PAN + 0x8;
#[allow(dead_code)]
const YM3812_WAVESEL: u16 = 0xE0;
const YMF262_4OP_EN: u16 = 0x104;
const YMF262_OPL3_EN: u16 = 0x105;

const YM3812_REG_KEY: u8 = 0x20;
const YM3812_REG_BLOCK: u8 = 0x1C;
const YM3812_REG_FNUM_MSB: u8 = 0x3;

#[allow(dead_code)]
const YM3812_REG_RHYTHM_MODE: u16 = 0x20;
const YM3812_REG_BD: u8 = 0x10;
const YM3812_REG_SD: u8 = 0x8;
const YM3812_REG_TOM: u8 = 0x4;
const YM3812_REG_TCT: u8 = 0x2;
const YM3812_REG_HH: u8 = 0x1;

pub(crate) fn hz_ym3812(fnum: f64, opt_block: Option<u8>, fsam_3812: f64) -> f64 {
	let block = opt_block.unwrap_or(0);

	if block == 0 && fnum == 0.0 {
		0.0
	} else {
		fnum * fsam_3812 * f64::powi(2.0, block as i32 - 20)
	}
}

pub(crate) struct YM3812State {
	fnum_msb: [u8; 8],
	key_on: [bool; 8],
	block: [u8; 8],
	fnum_lsb: [u8; 8],
	fnum_1: [u32; 8],
	fnum_2: [u32; 8],
	hz_1: [f64; 8],
	hz_2: [f64; 8],
	note_1: [f64; 8],
	note_2: [f64; 8],
	#[allow(dead_code)]
	instrument: [u8; 8],
	volume: [u8; 8],
	bd_vol: u8,
	hh_vol: u8,
	sd_vol: u8,
	tom_vol: u8,
	tct_vol: u8,
	midi_note: [u8; 8],
	midi_wheel: [u16; 8],
	#[allow(dead_code)]
	midi_volume: [u8; 8],
	midi_pan: [u8; 8],
	note_on_1: [bool; 8],
	note_on_2: [bool; 8],
	percussion_on: [bool; 5],
	opl3_mode: u8,
	pub opl_type: u8,
}

pub(crate) struct YM3812 {
	pub state: YM3812State,
	config: Config, //FIXME: This should be able to be a reference. It's basically static anyway though...
}

impl YM3812 {
	pub(crate) fn new(config: &Config, opt_state: Option<YM3812State>) -> Self {
		YM3812 {
			state: opt_state.unwrap_or(Default::default()),
			config: config.to_owned(),
		}
	}

	pub(crate) fn command_handle(
		&mut self,
		register: u16,
		data: u8,
		fsam_3812: f64,
		midi: &mut MIDIShim,
	) -> Result<()> {
		match register {
			YM3812_REG_TEST_WAVESEL_EN => (),
			//WaveselEnable = (Data & 0x20) / 0x20
			YMF262_4OP_EN => (),
			YMF262_OPL3_EN => self.state.opl3_mode = data & 0x1,
			YM3812_REG_CSW_NOTESEL => (),
			YM3812_REG_MOD_VIB_EG_KS_MULT..=YM3812_REG_MOD_VIB_EG_KS_MULT_END => {
				process_mod_vib_eg_ks_mult(register, data, &self.config, midi)
			},
			YM3812_REG_KSL_TL..=YM3812_REG_KSL_TL_END => {
				ksl_tl(self, register, data, midi)
			},
			YM3812_REG_FNUM_LSB..=YM3812_REG_FNUM_LSB_END
			| YM3812_REG_KEY_BLOCK_FNUM_MSB..=YM3812_REG_KEY_BLOCK_FNUM_MSB_END => {
				let channel = (register & 0xF) as u8;

				if channel > 0x8 {
					return Ok(());
				}
				if self.config.ym2413_ch_disabled[channel as usize] {
					return Ok(());
				}

				if (register & 0xF0) == YM3812_REG_FNUM_LSB {
					self.state.fnum_lsb[channel as usize] = data;
				//Exit Sub
				} else if (register & 0xF0) == YM3812_REG_KEY_BLOCK_FNUM_MSB {
					self.state.key_on[channel as usize] =
						(data & YM3812_REG_KEY) / YM3812_REG_KEY != 0;
					self.state.block[channel as usize] =
						(data & YM3812_REG_BLOCK) / 0x4;
					self.state.fnum_msb[channel as usize] =
						data & YM3812_REG_FNUM_MSB;
				}

				self.state.fnum_1[channel as usize] =
					self.state.fnum_2[channel as usize];
				self.state.fnum_2[channel as usize] =
					(self.state.fnum_msb[channel as usize] as u32 * 256)
						+ self.state.fnum_lsb[channel as usize] as u32;

				self.state.hz_1[channel as usize] =
					self.state.hz_2[channel as usize];
				self.state.hz_2[channel as usize] = hz_ym3812(
					self.state.fnum_2[channel as usize] as f64,
					Some(self.state.block[channel as usize]),
					fsam_3812,
				);

				self.state.note_1[channel as usize] =
					self.state.note_2[channel as usize];
				self.state.note_2[channel as usize] =
					note(self.state.hz_2[channel as usize]);

				self.state.note_on_1[channel as usize] =
					self.state.note_on_2[channel as usize];
				if (register & 0xF0) == YM3812_REG_KEY_BLOCK_FNUM_MSB {
					self.state.note_on_2[channel as usize] =
						self.state.key_on[channel as usize]
				}

				if !self.state.key_on[channel as usize] {
					if self.state.note_on_1[channel as usize]
						&& self.state.note_1[channel as usize] != 0.0
					{
						midi.event_write(Midi {
							channel: channel.into(),
							message: NoteOff {
								key: self.state.midi_note
									[channel as usize]
									.into(),
								vel: 0x00.into(),
							},
						});
						self.state.midi_note[channel as usize] = 0xFF
					}
				} else if self.state.key_on[channel as usize] {
					if self.state.note_on_2[channel as usize]
						!= self.state.note_on_1[channel as usize]
					{
						do_note_on(
							self.state.note_1[channel as usize],
							self.state.note_2[channel as usize],
							channel,
							&mut self.state.midi_note[channel as usize],
							&mut self.state.midi_wheel
								[channel as usize],
							Some(255),
							None,
							&self.config,
							midi,
						);
					} else {
						//if (Register & 0xF0) = YM3812_REG_KEY_BLOCK_FNum_MSB & _
						//self.state.note_1[channel as usize] != self.state.note_2[channel as usize] {
						do_note_on(
							self.state.note_1[channel as usize],
							self.state.note_2[channel as usize],
							channel,
							&mut self.state.midi_note[channel as usize],
							&mut self.state.midi_wheel
								[channel as usize],
							None,
							None,
							&self.config,
							midi,
						);
					}
				}
			},
			YM3812_REG_RHYTHM => {
				process_reg_rhythm(data, &mut self.state, &self.config, midi)
			},
			YM3812_FB_CONNECTION_PAN..=YM3812_FB_CONNECTION_PAN_END => {
				process_fb_connection_pan(
					register,
					data,
					&mut self.state,
					&self.config,
					midi,
				);
			},
			_ => {
				strict!("Error when processing ym3812: {}, {}", register, data);
			},
		}
		Ok(())
	}
}

impl Default for YM3812State {
	fn default() -> Self {
		YM3812State {
			fnum_msb: [0; 8],
			key_on: [false; 8],
			block: [0; 8],
			fnum_lsb: [0; 8],
			fnum_1: [0; 8],
			fnum_2: [0; 8],
			hz_1: [0.0; 8],
			hz_2: [0.0; 8],
			note_1: [0.0; 8],
			note_2: [0.0; 8],
			instrument: [0; 8],
			volume: [0; 8],
			bd_vol: 0,
			hh_vol: 0,
			sd_vol: 0,
			tom_vol: 0,
			tct_vol: 0,
			midi_note: [0xFF; 8],
			midi_wheel: [0x8000; 8],
			midi_volume: [0xFF; 8],
			midi_pan: [0xFF; 8],
			note_on_1: [false; 8],
			note_on_2: [false; 8],
			percussion_on: [false; 5],
			opl3_mode: 0,
			opl_type: 0,
		}
	}
}

fn ksl_tl(me: &mut YM3812, register: u16, data: u8, midi: &mut MIDIShim) {
	if (register & 0x7) > 0x5 {}
	let operation = (register & 0x7) / 0x3;
	let channel = (((register & 0x18) / 0x8 * 0x3) + ((register & 0x7) % 0x3)) as u8;
	if me.config.ym2413_ch_disabled[channel as usize] {
		return;
	}
	match (channel, operation) {
		(0x6, 0x1) =>
		//me.state.bd_vol = OPL_TL2Vol(Data & 0x3F)
		{
			me.state.bd_vol = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F))
		},
		(0x7, 0x0) =>
		//me.state.hh_vol = OPL_TL2Vol(Data & 0x3F)
		{
			me.state.hh_vol = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F))
		},
		(0x7, 0x1) =>
		//me.state.sd_vol = OPL_TL2Vol(Data & 0x3F)
		{
			me.state.sd_vol = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F))
		},
		(0x8, 0x0) =>
		//me.state.tom_vol = OPL_TL2Vol(Data & 0x3F)
		{
			me.state.tom_vol = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F))
		},
		(0x8, 0x1) =>
		//me.state.tct_vol = OPL_TL2Vol(Data & 0x3F)
		{
			me.state.tct_vol = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F))
		},
		(_, _) => (),
	}
	if me.config.ym2413_vol_disabled[channel as usize] || operation == 0x0 {
		return;
	}
	let temp_byte = db_to_midi_vol(ym3812_vol_to_db(data & 0x3F));

	//TempByt = OPL_TL2Vol(Data & 0x3F)

	if me.state.volume[channel as usize] != temp_byte {
		me.state.volume[channel as usize] = temp_byte;
		midi.event_write(Midi {
			channel: channel.into(),
			message: Controller {
				controller: MIDI_VOLUME,
				value: me.state.volume[channel as usize].into(),
			},
		});
	}
}

fn process_fb_connection_pan(
	register: u16,
	data: u8,
	state: &mut YM3812State,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let channel = register as u8 & 0xF;
	if config.ym2413_ch_disabled[channel as usize] {
		return;
	}
	if config.ym2413_vol_disabled[channel as usize] {
		return;
	}
	if state.opl_type != OPL_TYPE_YMF262 {
		return;
	}
	let temp_byte = match (state.opl3_mode, (data & 0x30) / 0x10) {
		(_, 0x0) => MIDI_PAN_CENTER, // should actually be complete silence
		(_, 0x1) => MIDI_PAN_LEFT,
		(_, 0x2) => MIDI_PAN_CENTER,
		(_, 0x3) => MIDI_PAN_RIGHT,
		(0x0, _) => MIDI_PAN_CENTER,
		(_, _) => MIDI_PAN_CENTER, //FIXME: this is likely incorrect
	};
	state.midi_pan[channel as usize] = temp_byte.into();
	midi.event_write(Midi {
		channel: channel.into(),
		message: Controller {
			controller: MIDI_PAN,
			value: state.midi_pan[channel as usize].into(),
		},
	});
}

fn process_reg_rhythm(data: u8, state: &mut YM3812State, config: &Config, midi: &mut MIDIShim) {
	if config.ym2413_percussion_disabled {
		return;
	}
	let channel = CHN_DAC;

	toggle_drum(
		data,
		YM3812_REG_BD,
		&mut state.percussion_on[0],
		midi,
		channel,
		0x23,
		state.bd_vol,
	);
	toggle_drum(
		data,
		YM3812_REG_SD,
		&mut state.percussion_on[1],
		midi,
		channel,
		0x26,
		state.sd_vol,
	);
	toggle_drum(
		data,
		YM3812_REG_TOM,
		&mut state.percussion_on[2],
		midi,
		channel,
		0x2D,
		state.tom_vol,
	);
	toggle_drum(
		data,
		YM3812_REG_TCT,
		&mut state.percussion_on[3],
		midi,
		channel,
		0x33,
		state.tct_vol,
	);
	toggle_drum(
		data,
		YM3812_REG_HH,
		&mut state.percussion_on[4],
		midi,
		channel,
		0x2A,
		state.hh_vol,
	);
}

fn toggle_drum(
	data: u8,
	reg: u8,
	percussion_on: &mut bool,
	midi: &mut MIDIShim,
	channel: u8,
	key: u8,
	drum_vol: u8,
) {
	if (data & reg) == 0 {
		if *percussion_on {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: key.into(),
					vel: 0x00.into(),
				},
			});
		}
		*percussion_on = false;
	} else {
		if !*percussion_on {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 35, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: key.into(),
					vel: drum_vol.into(),
				},
			});
			*percussion_on = true;
		}
	}
}

fn process_mod_vib_eg_ks_mult(register: u16, data: u8, config: &Config, midi: &mut MIDIShim) {
	if (register & 0x7) > 0x5 {
		return;
	}

	let operation = (register & 0x7) / 0x3;
	let channel = ((register & 0x18) / 0x8 * 0x3 + (register & 0x7) % 0x3) as u8;
	let temp_byte = (data & 0xFE) / 0x2;

	if config.ym2413_ch_disabled[channel as usize]
		|| config.ym2413_vol_disabled[channel as usize]
		|| operation == 0x00
	{
		return;
	}

	midi.event_write(Midi {
		channel: channel.into(),
		message: ProgramChange {
			program: temp_byte.into(),
		},
	});
}

pub(crate) fn ym3812_vol_to_db(tl: u8) -> f64 {
	-f64::from(tl) * 4.0 / 3.0
}
