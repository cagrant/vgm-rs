use crate::config::Config;
use crate::midi_shim::{db_to_midi_vol, MIDIShim, MIDI_VOLUME};
use crate::vgm2mid::{do_note_on, note, CHN_DAC};
use midly::MidiMessage::Controller;
use midly::MidiMessage::NoteOff;
use midly::MidiMessage::NoteOn;
use midly::MidiMessage::ProgramChange;
use midly::TrackEventKind::Midi;

// YM2413 Register Constants
#[allow(dead_code)]
pub(crate) const YM2413_REG_USER: u8 = 0x00;

pub(crate) const YM2413_REG_RHYTHM: u8 = 0x0E;
pub(crate) const YM2413_REG_RHYTHM_MODE: u8 = 0x20;
pub(crate) const YM2413_REG_BD: u8 = 0x10;
pub(crate) const YM2413_REG_SD: u8 = 0x08;
pub(crate) const YM2413_REG_TOM: u8 = 0x04;
pub(crate) const YM2413_REG_TCT: u8 = 0x02;
pub(crate) const YM2413_REG_HH: u8 = 0x01;

#[allow(dead_code)]
pub(crate) const YM2413_REG_TEST: u8 = 0x0F;
pub(crate) const YM2413_REG_FNUM_LSB: u8 = 0x10;
pub(crate) const YM2413_REG_FNUM_LSB_END: u8 = YM2413_REG_FNUM_LSB + 8;

pub(crate) const YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB: u8 = 0x20;
pub(crate) const YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB_END: u8 = YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB + 8;
pub(crate) const YM2413_REG_SUS: u8 = 0x20;
pub(crate) const YM2413_REG_KEY: u8 = 0x10;
pub(crate) const YM2413_REG_BLOCK: u8 = 0x0E;
pub(crate) const YM2413_REG_FNUM_MSB: u8 = 0x01;

pub(crate) const YM2413_REG_INST_VOL: u8 = 0x30;
pub(crate) const YM2413_REG_INST_VOL_5: u8 = YM2413_REG_INST_VOL + 5;
pub(crate) const YM2413_REG_INST_VOL_6: u8 = YM2413_REG_INST_VOL + 6;
pub(crate) const YM2413_REG_INST_VOL_8: u8 = YM2413_REG_INST_VOL + 8;
pub(crate) const YM2413_REG_INST: u8 = 0xF0;
pub(crate) const YM2413_REG_VOL: u8 = 0x0F;

// Tone Data (INST) Constants
pub(crate) const INST_ORIGINAL: u8 = 0x00;
pub(crate) const INST_VIOLIN: u8 = 0x01;
pub(crate) const INST_GUITAR: u8 = 0x02;
pub(crate) const INST_PIANO: u8 = 0x03;
pub(crate) const INST_FLUTE: u8 = 0x04;
pub(crate) const INST_CLARINET: u8 = 0x05;
pub(crate) const INST_OBOE: u8 = 0x06;
pub(crate) const INST_TRUMPET: u8 = 0x07;
pub(crate) const INST_ORGAN: u8 = 0x08;
pub(crate) const INST_HORN: u8 = 0x09;
pub(crate) const INST_SYNTHESIZER: u8 = 0x0A;
pub(crate) const INST_HARPSICHORD: u8 = 0x0B;
pub(crate) const INST_VIBRAPHONE: u8 = 0x0C;
pub(crate) const INST_BASS_SYNTH: u8 = 0x0D;
pub(crate) const INST_BASS_ACOUSTIC: u8 = 0x0E;
pub(crate) const INST_GUITAR_ELECTRIC: u8 = 0x0F;

pub(crate) fn hz_ym2413(fnum: f64, opt_block: Option<u8>, clock: u32) -> f64 {
	let block = opt_block.unwrap_or(0);

	if block == 0 && fnum == 0.0 {
		0.0
	} else {
		fnum * (f64::from(clock) / 72.0) * 2.0_f64.powi(block as i32 - 19)
	}
}

pub(crate) struct YM2413State {
	fnum_msb: [u8; 8],
	sus_on: [bool; 8],
	key_on: [bool; 8],
	block: [u8; 8],
	fnum_lsb: [u8; 8],
	fnum_1: [u32; 8],
	fnum_2: [u32; 8],
	hz_1: [f64; 8],
	hz_2: [f64; 8],
	note_1: [f64; 8],
	note_2: [f64; 8],
	ins_vol: [u8; 8],
	instrument: [u8; 8],
	volume: [u8; 8],
	bd_vol: u8,
	hh_vol: u8,
	sd_vol: u8,
	tom_vol: u8,
	tct_vol: u8,
	midi_note: [u8; 8],
	midi_wheel: [u16; 8],
	midi_volume: [u8; 8],
	note_on_1: [bool; 8],
	note_on_2: [bool; 8],
	percussion_on: [bool; 5],
}

impl Default for YM2413State {
	fn default() -> Self {
		YM2413State {
			fnum_msb: [0; 8],
			sus_on: [false; 8],
			key_on: [false; 8],
			block: [0; 8],
			fnum_lsb: [0; 8],
			fnum_1: [0; 8],
			fnum_2: [0; 8],
			hz_1: [0.0; 8],
			hz_2: [0.0; 8],
			note_1: [0.0; 8],
			note_2: [0.0; 8],
			ins_vol: [0; 8],
			instrument: [0xFF; 8],
			volume: [0xFF; 8],
			bd_vol: 0,
			hh_vol: 0,
			sd_vol: 0,
			tom_vol: 0,
			tct_vol: 0,
			midi_note: [0xFF; 8],
			midi_wheel: [0x8000; 8],
			midi_volume: [0xFF; 8],
			note_on_1: [false; 8],
			note_on_2: [false; 8],
			percussion_on: [false; 5],
		}
	}
}

pub(crate) fn ym2413_command_handle(
	register: u8,
	data: u8,
	state: &mut YM2413State,
	config: &Config,
	clock: u32,
	midi: &mut MIDIShim,
) {
	/*if Variables_Clear_YM2413 = 1 {
		CH = 0
		Erase state.FNum_MSB
		Erase state.SUS_ON: Erase state.KEY_ON:  Erase block: Erase state.FNum_LSB: Erase state.FNum_1: Erase state.FNum_2: Erase state.Hz_1: Erase state.Hz_2: Erase state.Note_1: Erase state.Note_2
		Erase state.InsVol: Erase state.Instrument: Erase state.Volume: state.BD_VOL = 0x7F: state.HH_VOL = 0x7F: state.SD_VOL = 0x7F: state.TOM_VOL = 0x7F: state.TCT_VOL = 0x7F
		Erase state.MIDINote: Erase state.MIDIWheel: Erase state.midi_volume: Erase state.NoteOn_1: Erase state.NoteOn_2: Erase state.Percussion_on
		For CH = 0x0..=0x8
			state.InsVol[CH as usize] = 0x0
			state.Instrument[CH as usize] = 0xFF
			state.Volume[CH as usize] = 0xFF
			state.MIDINote[CH as usize] = 0xFF
			state.MIDIWheel[CH as usize] = 0x8000
			state.midi_volume[CH as usize] = 0xFF
		Next CH
		Variables_Clear_YM2413 = 0
	}*/

	match register {
		//	Case YM2413_REG_USER..=(YM2413_REG_USER + 7)
		YM2413_REG_RHYTHM => {
			fun_name(state, data, config, midi);
		},
		YM2413_REG_FNUM_LSB..=YM2413_REG_FNUM_LSB_END
		| YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB..=YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB_END => {
			fun_name1(register, state, data, config, clock, midi);
		},
		YM2413_REG_INST_VOL..=YM2413_REG_INST_VOL_5 => {
			fun_name2(register, state, data, config, midi);
		},
		YM2413_REG_INST_VOL_6..=YM2413_REG_INST_VOL_8 => {
			fun_name3(register, state, data, config);
		},
		_ => panic!(),
	}
}

fn fun_name3(register: u8, state: &mut YM2413State, data: u8, config: &Config) {
	if config.ym2413_percussion_disabled {
		return;
	}

	match register - YM2413_REG_INST_VOL {
		6 => {
			////state.BD_VOL = (MIDI_VOLUME_MAX - ((Data & 15) * 8)) * 0.8
			//state.BD_VOL = (MIDI_VOLUME_MAX - ((Data & 0xF) * 0x8)) + 1
			//if state.BD_VOL = 0x80 { state.BD_VOL = 0x7F
			state.bd_vol = db_to_midi_vol(ym2413_vol_to_db(data & 0xF));
		},
		7 => {
			//state.HH_VOL = MIDI_VOLUME_MAX - (((Data & 0xF0) / 0x10) * 8) + 1
			//if state.HH_VOL = 0x80 { state.HH_VOL = 0x7F
			state.hh_vol = db_to_midi_vol(ym2413_vol_to_db((data & 0xF0) / 0x10));
			////state.SD_VOL = (MIDI_VOLUME_MAX - ((Data & 15) * 8)) * 0.8
			//state.SD_VOL = (MIDI_VOLUME_MAX - ((Data & 0xF) * 0x8)) + 1
			//if state.SD_VOL = 0x80 { state.SD_VOL = 0x7F
			state.sd_vol = db_to_midi_vol(ym2413_vol_to_db(data & 0xF));
		},
		8 => {
			////state.TOM_VOL = (MIDI_VOLUME_MAX - (((Data & 240) / 16) * 8)) * 0.6
			//state.TOM_VOL = (MIDI_VOLUME_MAX - (((Data & 0xF0) / 0x10) * 8)) + 1
			//if state.TOM_VOL = 0x80 { state.TOM_VOL = 0x7F
			state.tom_vol = db_to_midi_vol(ym2413_vol_to_db((data & 0xF0) / 0x10));
			//state.TCT_VOL = MIDI_VOLUME_MAX - ((Data & 0xF) * 0x8) + 1
			//if state.TCT_VOL = 0x80 { state.TCT_VOL = 0x7F
			state.tct_vol = db_to_midi_vol(ym2413_vol_to_db(data & 0xF));
		},
		_ => panic!(),
	}
}

fn fun_name2(
	register: u8,
	state: &mut YM2413State,
	data: u8,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let channel = register - YM2413_REG_INST_VOL;

	let mut temp_byte = state.instrument[channel as usize];
	state.instrument[channel as usize] = (data & YM2413_REG_INST) / 16;
	if !config.ym2413_prog_disabled[channel as usize]
		&& (state.ins_vol[channel as usize] == data
			|| temp_byte != state.instrument[channel as usize])
	{
		match state.instrument[channel as usize] {
			INST_ORIGINAL => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[0].into(),
					},
				});
			},
			INST_VIOLIN => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[1].into(),
					},
				});
			},
			INST_GUITAR => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[2].into(),
					},
				});
			},
			INST_PIANO => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[3].into(),
					},
				});
			},
			INST_FLUTE => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[4].into(),
					},
				});
			},
			INST_CLARINET => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[5].into(),
					},
				});
			},
			INST_OBOE => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[6].into(),
					},
				});
			},
			INST_TRUMPET => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[7].into(),
					},
				});
			},
			INST_ORGAN => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[8].into(),
					},
				});
			},
			INST_HORN => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[9].into(),
					},
				});
			},
			INST_SYNTHESIZER => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[10].into(),
					},
				});
			},
			INST_HARPSICHORD => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[11].into(),
					},
				});
			},
			INST_VIBRAPHONE => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[12].into(),
					},
				});
			},
			INST_BASS_SYNTH => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[13].into(),
					},
				});
			},
			INST_BASS_ACOUSTIC => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[14].into(),
					},
				});
			},
			INST_GUITAR_ELECTRIC => {
				midi.event_write(Midi {
					channel: channel.into(),
					message: ProgramChange {
						program: config.ym2413_midi_patch[15].into(),
					},
				});
			},
			_ => panic!(),
		}
	}

	temp_byte = state.volume[channel as usize];
	state.volume[channel as usize] = data & YM2413_REG_VOL;
	if config.ym2413_vol_disabled[channel as usize]
		&& (state.ins_vol[channel as usize] == data
			|| temp_byte != state.volume[channel as usize])
	{
		//state.Volume[CH as usize] = MIDI_VOLUME_MAX - ((Data & YM2413_REG_VOL) * 8) + 1
		//if state.Volume[CH as usize] = 0x80 { state.Volume[CH as usize] = 0x7F
		state.midi_volume[channel as usize] =
			db_to_midi_vol(ym2413_vol_to_db(state.volume[channel as usize]));
		midi.event_write(Midi {
			channel: channel.into(),
			message: Controller {
				controller: MIDI_VOLUME,
				value: state.midi_volume[channel as usize].into(),
			},
		});
	}
	state.ins_vol[channel as usize] = data;
}

fn fun_name1(
	register: u8,
	state: &mut YM2413State,
	data: u8,
	config: &Config,
	clock: u32,
	midi: &mut MIDIShim,
) {
	let channel = register & 0xF;
	if config.ym2413_ch_disabled[channel as usize] {
		return;
	}
	if (register & 0xF0) == YM2413_REG_FNUM_LSB {
		state.fnum_lsb[channel as usize] = data;
		if !config.ym2413_optimized_vgms {
			return;
		}
	} else if (register & 0xF0) == YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB {
		state.sus_on[channel as usize] = data & YM2413_REG_SUS == 0;
		state.key_on[channel as usize] = data & YM2413_REG_KEY == 0;
		state.block[channel as usize] = (data & YM2413_REG_BLOCK) / 2;
		state.fnum_msb[channel as usize] = data & YM2413_REG_FNUM_MSB;
	}
	state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
	state.fnum_2[channel as usize] = (state.fnum_msb[channel as usize] as u32 * 256)
		+ state.fnum_lsb[channel as usize] as u32;
	state.hz_1[channel as usize] = state.hz_2[channel as usize];
	state.hz_2[channel as usize] = hz_ym2413(
		state.fnum_2[channel as usize] as f64,
		Some(state.block[channel as usize]),
		clock,
	);
	state.note_1[channel as usize] = state.note_2[channel as usize];
	state.note_2[channel as usize] = note(state.hz_2[channel as usize]);
	state.note_on_1[channel as usize] = state.note_on_2[channel as usize];
	if (register & 0xF0) == YM2413_REG_SUS_KEY_BLOCK_FNUM_MSB {
		state.note_on_2[channel as usize] = state.key_on[channel as usize];
	}
	//CH = Register - YM2413_REG_SUS_KEY_BLOCK_state.FNum_MSB

	if !state.key_on[channel as usize] {
		if state.note_on_1[channel as usize] && state.note_1[channel as usize] != 0.0 {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: state.midi_note[channel as usize].into(),
					vel: 0x00.into(),
				},
			});
			state.midi_note[channel as usize] = 0xFF;
		}
	} else if state.key_on[channel as usize] {
		if state.note_on_2[channel as usize] != state.note_on_1[channel as usize] {
			do_note_on(
				state.note_1[channel as usize],
				state.note_2[channel as usize],
				channel,
				&mut state.midi_note[channel as usize],
				&mut state.midi_wheel[channel as usize],
				Some(255),
				None,
				config,
				midi,
			);
		} else {
			//if (Register & 0xF0) = YM2413_REG_SUS_KEY_BLOCK_state.FNum_MSB & _
			//state.Note_1[CH as usize] != state.Note_2[CH as usize] {
			do_note_on(
				state.note_1[channel as usize],
				state.note_2[channel as usize],
				channel,
				&mut state.midi_note[channel as usize],
				&mut state.midi_wheel[channel as usize],
				None,
				None,
				config,
				midi,
			);
		}
	}
}

fn fun_name(state: &mut YM2413State, mut data: u8, config: &Config, midi: &mut MIDIShim) {
	if config.ym2413_percussion_disabled {
		return;
	}

	let channel = CHN_DAC;

	if (data & YM2413_REG_RHYTHM_MODE) == 0 {
		data = 0x0;
	}
	if (data & YM2413_REG_BD) == 0 {
		if state.percussion_on[0] {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: 0x23.into(),
					vel: 0x00.into(),
				},
			});
			state.percussion_on[0] = false;
		}
	} else {
		if !state.percussion_on[0] {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 35, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: 0x23.into(),
					vel: state.bd_vol.into(),
				},
			});
			state.percussion_on[0] = true;
		}
	}
	if (data & YM2413_REG_SD) == 0 {
		if state.percussion_on[1] {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: 0x26.into(),
					vel: 0x00.into(),
				},
			});
			state.percussion_on[1] = false;
		}
	} else {
		if !state.percussion_on[1] {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 38, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: 0x26.into(),
					vel: state.sd_vol.into(),
				},
			});
			state.percussion_on[1] = true;
		}
	}
	if (data & YM2413_REG_TOM) == 0 {
		if state.percussion_on[2] {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: 0x2D.into(),
					vel: 0x00.into(),
				},
			});
			state.percussion_on[2] = false;
		}
	} else {
		if !state.percussion_on[2] {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 45, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: 0x2D.into(),
					vel: state.tom_vol.into(),
				},
			});
			state.percussion_on[2] = true;
		}
	}
	if (data & YM2413_REG_HH) == 0 {
		if state.percussion_on[4] {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: 0x2A.into(),
					vel: 0x00.into(),
				},
			});
			state.percussion_on[4] = false;
		}
	} else {
		if !state.percussion_on[4] {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 42, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: 0x2A.into(),
					vel: state.hh_vol.into(),
				},
			});
			state.percussion_on[4] = true;
		}
	}

	// Note Value 46 (Open HiHat) replaced with 51 (Ride Cymbal 1),
	// because REG_TCT is often used with REG_HH at the same time and
	// prevents the Cymbal from playing

	//	Case YM2413_REG_TEST

	//Case YM2413_REG_state.FNum_LSB..=(YM2413_REG_state.FNum_LSB + 8)
	//CH = Register - YM2413_REG_state.FNum_LSB
	//
	//if config.ym2413_ch_disabled[CH as usize] = 1 { return }
	//
	//state.FNum_LSB[CH as usize] = Data

	//Case YM2413_REG_SUS_KEY_BLOCK_state.FNum_MSB..=(YM2413_REG_SUS_KEY_BLOCK_state.FNum_MSB + 8)
	if (data & YM2413_REG_TCT) == 0 {
		if state.percussion_on[3] {
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOff {
					key: 0x2E.into(),
					vel: 0x00.into(),
				},
			});
			state.percussion_on[3] = false;
		}
	} else {
		if !state.percussion_on[3] {
			//midi.event_write(Midi { channel: CH, message: NoteOff { key: 51, vel: 0x0 }});
			midi.event_write(Midi {
				channel: channel.into(),
				message: NoteOn {
					key: 0x2E.into(),
					vel: state.tct_vol.into(),
				},
			});
			state.percussion_on[3] = true;
		}
	}
}

pub(crate) fn ym2413_vol_to_db(tl: u8) -> f64 {
	-f64::from(tl) * 3.0
}
