pub mod ay8910;
pub mod cli;
pub mod config;
pub mod gameboy;
pub mod gd3;
pub mod gym;
pub mod midi_shim;
pub mod nesapu;
pub mod pcm;
pub mod sn76489;
pub mod vgm;
pub mod vgm2mid;
pub mod ym2151;
pub mod ym2413;
pub mod ym2612;
pub mod ym3812;
pub mod ymf278;
pub mod utils;

use crate::config::Config;
use crate::gym::convert_gym_to_mid;
use crate::vgm::convert_vgm_to_mid;
use anyhow::{anyhow, Context, Result};
use flate2::bufread::GzDecoder;
use once_cell::sync::OnceCell;
use std::io::Read;
use std::path::Path;

#[macro_export]
macro_rules! verbose {
	($($arg:tt)*) => {
		if $crate::VERBOSE.get().map(|r| *r).unwrap_or(false) {
			std::println!($($arg)*);
		}
	}
}

#[macro_export]
macro_rules! strict {
	() => {
		if $crate::STRICT.get().map(|r| *r).unwrap_or(false) {
			anyhow::bail!("Strict violation.");
		}
	};
	($($arg:tt)*)  => {
		$crate::verbose!($($arg)*);
		if $crate::STRICT.get().map(|r| *r).unwrap_or(false) {
			anyhow::bail!("Strict violation.");
		}
	};
}

pub(crate) enum FileType {
	GYM,
	VGM,
	VGZ,
}

pub(crate) static CONFIG: OnceCell<Config> = OnceCell::new();
pub(crate) static STRICT: OnceCell<bool> = OnceCell::new();
pub(crate) static VERBOSE: OnceCell<bool> = OnceCell::new();

fn main() -> Result<()> {
	let (in_path, out_path, config_path, file_type) = cli::cli()?;
	let config = config::load_config_or_default(config_path.as_ref().map(|pb| pb.as_path()))?;
	CONFIG.set(config)
		.map_err(|_| anyhow!("Failed to set static config"))?;
	convert_to_mid(
		&in_path,
		file_type,
		&out_path,
		&CONFIG.get().expect("Static CONFIG not set"),
	)
}

fn convert_to_mid(
	in_path: &Path,
	file_type: FileType,
	out_path: &Path,
	config: &Config,
) -> Result<()> {
	let _metadata = std::fs::metadata(in_path)
		.with_context(|| format!("Failed to load file metadata: {}", in_path.display()))?;

	let data = std::fs::read(in_path)
		.with_context(|| format!("Failed to load data from file: {}", in_path.display()))?;

	match file_type {
		FileType::GYM => convert_gym_to_mid(data.as_slice(), config),
		FileType::VGM => convert_vgm_to_mid(data.as_slice(), config, out_path),
		FileType::VGZ => {
			let mut buf = Vec::<u8>::new();
			GzDecoder::new(data.as_slice()).read_to_end(&mut buf)?;
			convert_vgm_to_mid(buf.as_slice(), config, out_path)
		},
	}
}
