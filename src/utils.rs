
fn shift<T: Copy>(buf_one: &mut T, buf_two: &mut T, new: T) -> T {
	let old = *buf_one;
	*buf_one = *buf_two;
	*buf_two = new;
	old
}
