pub(crate) use crate::{verbose, FileType, STRICT, VERBOSE};
use anyhow::{bail, Result};
use clap::{command, Arg, ArgAction};
use std::path::PathBuf;

pub(crate) fn cli() -> Result<(PathBuf, PathBuf, Option<PathBuf>, FileType)> {
	let matches = command!()
		.arg(
			Arg::new("verbose")
				.action(ArgAction::SetTrue)
				.long("verbose")
				.short('v')
				.help("Prints extra information and any recoverable errors to stderr."),
		)
		.arg(
			Arg::new("strict")
				.action(ArgAction::SetTrue)
				.long("strict")
				.short('s')
				.help("Aborts if any unknown data are encountered during processing."),
		)
		.arg(
			Arg::new("gym")
				.action(ArgAction::SetTrue)
				.long("gym")
				.help("Sets the filetype to gym."),
		)
		.arg(
			Arg::new("vgm")
				.action(ArgAction::SetTrue)
				.conflicts_with("gym")
				.long("vgm")
				.help("Sets the filetype to vgm."),
		)
		.arg(
			Arg::new("zipped")
				.action(ArgAction::SetTrue)
				.requires("vgm")
				.conflicts_with("gym")
				.long("zipped")
				.short('z')
				.help("Extracts gzipped vgms (.vgz)"),
		)
		.arg(
			Arg::new("config")
				.long("config")
				.help("Path to the desired config file."),
		)
		.arg(Arg::new("in").required(true).help(
			"The file to be processed. Filetype is inferred by extension if not set with flags.",
		))
		.arg(
			Arg::new("out")
				.required(true)
				.help("The path to a resulting midi file. Existing files will not be overwritten."),
		)
		.get_matches();

	let in_path = PathBuf::from(matches.get_one::<String>("in").expect("Required"));

	let out_path = PathBuf::from(matches.get_one::<String>("out").expect("Required"));

	VERBOSE.set(matches.get_flag("verbose"))
		.expect("Static OnceCell VERBOSE already set before cli.");
	STRICT.set(matches.get_flag("strict"))
		.expect("Static OnceCell STRICT already set before cli.");

	let config_path = matches
		.get_one::<PathBuf>("config")
		.map(|pb| pb.to_path_buf());

	let file_type = match (
		matches.get_flag("gym"),
		matches.get_flag("vgm"),
		matches.get_flag("zipped"),
	) {
		(true, false, false) => Ok(FileType::GYM),
		(false, false, false) => determine_file_type(&in_path),
		(false, true, false) => Ok(FileType::VGM),
		(false, true, true) => Ok(FileType::VGZ),
		_ => bail!("Illegal flag combination"),
	}?;

	Ok((in_path, out_path, config_path, file_type))
}

fn determine_file_type(in_path: &PathBuf) -> Result<FileType> {
	verbose!("File type flag not set. Attempting to determine file type.");
	match in_path.extension().map(|osstr| osstr.to_str()).flatten() {
		Some("gym") => {
			verbose!("Determined file type to be gym from {}", in_path.display());
			Ok(FileType::GYM)
		},
		Some("vgm") => {
			verbose!("Determined file type to be vgm from {}", in_path.display());
			Ok(FileType::VGM)
		},
		Some("vgz") => {
			verbose!("Determined file type to be vgz from {}", in_path.display());
			Ok(FileType::VGZ)
		},
		_ => bail!("Unrecognized file extension from {}", in_path.display()),
	}
}

pub fn vgm_convert() -> i64 {
	unimplemented!()
	/*let (file_name, file_ext): (String, String);
	let midfilename: String;

	let vgm = unimplemented!();
	mid_file_init();
	match vgm.FileExt {
		"vgm" | "vgz" | "gym" => {
			vgm.ConvertToMID();
		},
		_ => eprintln!("Invalid file format."),
	}
	mid_file_write(midfilename);*/
}
/*
strFilename = Trim(File_Open(Me, "Please select a file", _
"All Supported Files", "*.vgm;*.gym;*.vgz;*.vgm.gz", _
"VGM Log (*.vgm;*.vgz;*.vgm.gz)", "*.vgm;*.vgz;*.vgm.gz", _
"GYM Log (*.gym)", "*.gym*"))
*/
