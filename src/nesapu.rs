use crate::config::Config;
use crate::midi_shim::db_to_midi_vol;
use crate::midi_shim::lin_to_db;
use crate::midi_shim::MIDIShim;
use crate::midi_shim::MIDI_VOLUME;
use crate::sn76489::MIDI_CHANNEL_SN76489_BASE;
use crate::vgm2mid::do_note_on;
use crate::vgm2mid::note;
use crate::vgm2mid::CHN_DAC;
use midly::MidiMessage::Controller;
use midly::MidiMessage::NoteOff;
use midly::MidiMessage::NoteOn;
use midly::MidiMessage::PitchBend;
use midly::MidiMessage::ProgramChange;
use midly::TrackEventKind::Midi;

const APU_WRA0: u8 = 0x0;
const APU_WRA1: u8 = 0x1;
const APU_WRA2: u8 = 0x2;
const APU_WRA3: u8 = 0x3;
const APU_WRB0: u8 = 0x4;
const APU_WRB1: u8 = 0x5;
const APU_WRB2: u8 = 0x6;
const APU_WRB3: u8 = 0x7;
const APU_WRC0: u8 = 0x8;
const APU_WRC2: u8 = 0xA;
const APU_WRC3: u8 = 0xB;
const APU_WRD0: u8 = 0xC;
const APU_WRD2: u8 = 0xE;
const APU_WRD3: u8 = 0xF;
const APU_WRE0: u8 = 0x10;
const APU_WRE1: u8 = 0x11;
const APU_WRE2: u8 = 0x12;
#[allow(dead_code)]
const APU_WRE3: u8 = 0x13;
const APU_SMASK: u8 = 0x15;
#[allow(dead_code)]
const APU_IRQCTRL: u8 = 0x17;

// N2A03 clock: 21 477 270 / 12 = 1 789 772.5
//N2A03 clock / 16
const NES_CLK_BASE: f64 = 111860.78125;

fn hz_nes(fnum: u32) -> f64 {
	NES_CLK_BASE / f64::from(fnum + 1)
}

fn hz_nesnoise(freq_mode: u32) -> f64 {
	let fnum: u16;

	fnum = [
		4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 2046,
	][(1 + freq_mode) as usize];
	NES_CLK_BASE / f64::from(fnum + 1)
}

pub(crate) struct NESAPUState {
	fnum_msb: [u8; 4],
	fnum_lsb: [u8; 4],
	fnum_1: [u32; 4],
	fnum_2: [u32; 4],
	hz_1: [f64; 4],
	hz_2: [f64; 4],
	note_1: [f64; 4],
	note_2: [f64; 4],
	envelope_1: [u8; 4],
	envelope_2: [u8; 4],
	vblen_1: [u8; 4],
	vblen_2: [u8; 4],
	hold: [u8; 4],
	tri_len: u8,
	duty_1: [u8; 4],
	duty_2: [u8; 4],
	midi_note: [u8; 4],
	midi_wheel: [u16; 4],
	note_on_1: [u8; 4],
	note_on_2: [u8; 4],
	midi_volume: u8,
	note_en_1: [u8; 4],
	note_en_2: [u8; 4],
	note_delay: [u32; 8],
}

impl Default for NESAPUState {
	fn default() -> Self {
		NESAPUState {
			fnum_msb: [0; 4],
			fnum_lsb: [0; 4],
			fnum_1: [0; 4],
			fnum_2: [0; 4],
			hz_1: [0.0; 4],
			hz_2: [0.0; 4],
			note_1: [0xFF.into(); 4],
			note_2: [0xFF.into(); 4],
			envelope_1: [0; 4],
			envelope_2: [0xFF; 4],
			vblen_1: [0; 4],
			vblen_2: [0; 4],
			hold: [0; 4],
			tri_len: 0,
			duty_1: [0; 4],
			duty_2: [0xFF; 4],
			midi_note: [0xFF; 4],
			midi_wheel: [0x8000; 4],
			note_on_1: [0; 4],
			note_on_2: [0; 4],
			midi_volume: 0,
			note_en_1: [0; 4],
			note_en_2: [0, 0, 4, 0],
			note_delay: [0; 8],
		}
	}
}

pub(crate) fn nes_apu_command_handle(
	register: u8,
	data: u8,
	state: &mut NESAPUState,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let mut midi_channel: u8;

	/*if Variables_Clear_YM2612 = 1 {
		Erase FNum_1: Erase FNum_2: Erase Hz_1: Erase Hz_2: Erase Note_1: Erase Note_2
		Erase Envelope_1: Erase Envelope_2: Erase VBLen_1: Erase VBLen_2: Erase Hold
		Erase state.Duty1: Erase state.Duty2: Erase NoteOn_1: Erase NoteOn_2
		Erase MIDINote: Erase MIDIWheel: Erase NoteOn_1: Erase NoteOn_2: MIDIVolume = 0
		for CH in 0x0..=0x4 {
			Envelope_2[channel as usize] = 0xFF;
			VBLen_2[channel as usize] = 0x0;
			Hold[channel as usize] = 0x0;
			state.Duty2[channel as usize] = 0xFF;
			state.note_1[channel as usize] = 0xFF;
			state.note_2[channel as usize] = 0xFF;
			NoteEn_2[channel as usize] = 0x0 | if(CH = 0x2, 0x0, 0x4);
			state.note_on_2[channel as usize] = 0x0;
			state.midi_note[channel as usize] = 0xFF;
			state.midi_wheel[channel as usize] = 0x8000;
		}
		TriLen = 0x0;
		Variables_Clear_YM2612 = 0;

		midi.event_write(Midi { channel: MIDI_CHANNEL_PSG_BASE + 0x2, message: Controller { controller: 0x0, value: 0x8; }});
		midi.event_write(MIDI_PROGRAM_CHANGE, MIDI_CHANNEL_PSG_BASE + 0x2, 0x50;);
		midi.event_write(MIDI_PROGRAM_CHANGE, MIDI_CHANNEL_PSG_BASE + 0x3, 0x7F;);
	}*/

	let channel = register / 0x4;
	if channel < 0x3 {
		if config.sn76489_ch_disabled[channel as usize] {
			return;
		}
	} else if channel == 0x3 {
		if config.sn76489_noise_disabled {
			return;
		}
	} else if channel == 0x4 {
		if config.ym2612_dac_disabled {
			return;
		}
	}

	midi_channel = if channel == 0x4 {
		CHN_DAC
	} else {
		MIDI_CHANNEL_SN76489_BASE + channel
	};
	match register {
		APU_WRA0 | APU_WRB0 | APU_WRD0 => {
			// Volume, Envelope, Hold, Duty Cycle
			if (register == APU_WRA0 || register == APU_WRB0) && (data & 0xF) > 0x0 {
				state.duty_1[channel as usize] = state.duty_2[channel as usize];
				state.duty_2[channel as usize] = (data & 0xC0) / 0x40;

				if state.duty_1[channel as usize] != state.duty_2[channel as usize]
				{
					let temp_byte =
						0x4F + (!state.duty_2[channel as usize] & 0x3);
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: ProgramChange {
							program: temp_byte.into(),
						},
					});
				}
			}

			state.envelope_1[channel as usize] = state.envelope_2[channel as usize];
			// output is 1 * envelope
			state.envelope_2[channel as usize] = data & 0xF;
			state.hold[channel as usize] = data & 0x20;

			if state.envelope_1[channel as usize] != state.envelope_2[channel as usize]
			{
				if state.envelope_2[channel as usize] == 0x0 {
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOff {
							key: state.midi_note[channel as usize]
								.into(),
							vel: 0x00.into(),
						},
					});
					state.midi_note[channel as usize] = 0xFF;
					state.note_delay[channel as usize] = 10000;
				}
				state.midi_volume = db_to_midi_vol(lin_to_db(
					state.envelope_2[channel as usize] as f64 / 0xF as f64,
				));
				midi.event_write(Midi {
					channel: midi_channel.into(),
					message: Controller {
						controller: MIDI_VOLUME,
						value: state.midi_volume.into(),
					},
				});
			}
		},
		APU_WRC0 => {
			state.hold[channel as usize] = data & 0x80;
			let temp_byte = data & 0x7F;
			if temp_byte != state.tri_len {
				state.tri_len = temp_byte;
				state.note_en_1[channel as usize] =
					state.note_en_2[channel as usize];
				state.note_en_2[channel as usize] = (state.note_en_2
					[channel as usize] & !0x4)
					| (state.tri_len & 0x4);
				if state.note_en_1[channel as usize]
					!= state.note_en_2[channel as usize] && (state.note_en_2
					[channel as usize]
					& 0x3) == 0x3
				{
					if state.note_en_2[channel as usize] & 0x4 != 0 {
						do_note_on(
							state.note_2[channel as usize],
							state.note_2[channel as usize],
							midi_channel,
							&mut state.midi_note[channel as usize],
							&mut state.midi_wheel[channel as usize],
							Some(255),
							None,
							config,
							midi,
						);
						state.note_delay[channel as usize] = 0;
					} else if state.midi_note[channel as usize] != 0xFF {
						// Note got silenced by setting TriLen = 0
						midi.event_write(Midi {
							channel: midi_channel.into(),
							message: NoteOff {
								key: state.midi_note
									[channel as usize]
									.into(),
								vel: 0x00.into(),
							},
						});
						state.midi_note[channel as usize] = 0xFF;
						state.note_delay[channel as usize] = 10000;
					}
				}
			}
		},
		APU_WRA1 | APU_WRB1 => (), // Sweep
		//FIXME: impossible to do in this version of vgm2mid
		APU_WRA2 | APU_WRB2 | APU_WRC2 | APU_WRA3 | APU_WRB3 | APU_WRC3 => {
			if (register & 0x3) == 0x2 {
				state.fnum_lsb[channel as usize] = data;
			//Exit Sub
			} else if (register & 0x3) == 0x3 {
				state.fnum_msb[channel as usize] = data;
				state.vblen_1[channel as usize] = state.vblen_2[channel as usize];
				if state.hold[channel as usize] != 0 {
					state.vblen_2[channel as usize] = 0xFF;
				} else {
					state.vblen_2[channel as usize] = (data & 0xF8) / 0x8;
				}
				state.note_en_1[channel as usize] =
					state.note_en_2[channel as usize];
				state.note_en_2[channel as usize] = (state.note_en_2
					[channel as usize] & !0x2)
					| state.vblen_2[channel as usize] & 0x2;
				if state.note_en_1[channel as usize]
					!= state.note_en_2[channel as usize] && (state.note_en_2
					[channel as usize]
					& 0x5) == 0x5
				{
					if state.note_en_2[channel as usize] & 0x2 == 0
						&& state.midi_note[channel as usize] != 0xFF
					{
						// Note got silenced by setting VBLen = 0
						midi.event_write(Midi {
							channel: midi_channel.into(),
							message: NoteOff {
								key: state.midi_note
									[channel as usize]
									.into(),
								vel: 0x00.into(),
							},
						});
						state.midi_note[channel as usize] = 0xFF;
						state.note_delay[channel as usize] = 10000;
					}
				}
			}
			state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
			state.fnum_2[channel as usize] =
				((state.fnum_msb[channel as usize] as u32 & 0x7) << 8)
					| (state.fnum_lsb[channel as usize] as u32 * 0x1);

			state.hz_1[channel as usize] = state.hz_2[channel as usize];
			state.hz_2[channel as usize] = hz_nes(state.fnum_2[channel as usize]);
			if channel == 0x2 {
				state.hz_2[channel as usize] = state.hz_2[channel as usize] / 2.0;
			}

			let mut temp_note = note(state.hz_2[channel as usize]);
			if temp_note >= 0x80.into() {
				//TempNote = 0x7F - state.fnum_2[channel as usize]
				temp_note = 0x7F.into();
			}
			state.note_1[channel as usize] = state.note_2[channel as usize];
			state.note_2[channel as usize] = temp_note;

			if (state.note_en_2[channel as usize] & 0x7) == 0x7 {
				if (register & 0x3) == 0x3
					&& state.note_delay[channel as usize] > 10
				{
					// writing to register 3 restarts the notes
					do_note_on(
						state.note_1[channel as usize],
						state.note_2[channel as usize],
						midi_channel,
						&mut state.midi_note[channel as usize],
						&mut state.midi_wheel[channel as usize],
						Some(255),
						None,
						config,
						midi,
					);
					state.note_delay[channel as usize] = 0;
				} else if state.note_1[channel as usize]
					!= state.note_2[channel as usize]
				{
					if do_note_on(
						state.note_1[channel as usize],
						state.note_2[channel as usize],
						midi_channel,
						&mut state.midi_note[channel as usize],
						&mut state.midi_wheel[channel as usize],
						None,
						None,
						config,
						midi,
					) {
						state.note_delay[channel as usize] = 0;
					}
				}
			}
		},
		APU_WRD2 => {
			// Noise Freq
			state.fnum_lsb[channel as usize] = data;
			state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
			state.fnum_2[channel as usize] =
				(state.fnum_lsb[channel as usize] & 0xF).into();

			state.hz_1[channel as usize] = state.hz_2[channel as usize];
			state.hz_2[channel as usize] = hz_nesnoise(state.fnum_2[channel as usize]);

			let mut temp_note = note(state.hz_2[channel as usize]);
			if temp_note >= 0x80.into() {
				//TempNote = 0x7F - state.fnum_2[channel as usize]
				temp_note = 0x7F.into();
			}
			state.note_1[channel as usize] = state.note_2[channel as usize];
			state.note_2[channel as usize] = temp_note;

			if (state.note_en_2[channel as usize] & 0x7) == 0x7
				&& state.note_1[channel as usize] != state.note_2[channel as usize]
			{
				if do_note_on(
					state.note_1[channel as usize],
					state.note_2[channel as usize],
					midi_channel,
					&mut state.midi_note[channel as usize],
					&mut state.midi_wheel[channel as usize],
					None,
					None,
					config,
					midi,
				) {
					state.note_delay[channel as usize] = 0;
				}
			}
		},
		APU_WRD3 => {
			state.vblen_1[channel as usize] = state.vblen_2[channel as usize];
			if state.hold[channel as usize] != 0 {
				state.vblen_2[channel as usize] = 1;
			} else {
				state.vblen_2[channel as usize] = (data & 0xF8) / 0x8;
			}
			state.note_en_1[channel as usize] = state.note_en_2[channel as usize];
			state.note_en_2[channel as usize] = (state.note_en_2[channel as usize]
				& !0x2) | (state.vblen_2
				[channel as usize]) & 0x2;
			if state.note_en_1[channel as usize] != state.note_en_2[channel as usize]
				&& (state.note_en_2[channel as usize] & 0x5) == 0x5
			{
				if !state.note_en_2[channel as usize] & 0x2 != 0
					&& state.midi_note[channel as usize] != 0xFF
				{
					// Note got silenced by setting VBLen = 0
					midi.event_write(Midi {
						channel: midi_channel.into(),
						message: NoteOff {
							key: state.midi_note[channel as usize]
								.into(),
							vel: 0x00.into(),
						},
					});
					state.midi_note[channel as usize] = 0xFF;
					state.note_delay[channel as usize] = 10000;
				}
			}

			if (state.note_en_2[channel as usize] & 0x7) == 0x7
				&& state.note_delay[channel as usize] > 10
			{
				// writing to register 3 restarts the notes
				do_note_on(
					state.note_2[channel as usize],
					state.note_2[channel as usize],
					midi_channel,
					&mut state.midi_note[channel as usize],
					&mut state.midi_wheel[channel as usize],
					Some(255),
					None,
					config,
					midi,
				);
				state.note_delay[channel as usize] = 0;
			}
		},
		APU_WRE0 => {
			// IRQ, Looping, Frequency
			state.fnum_lsb[channel as usize] = data;
			state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
			state.fnum_2[channel as usize] =
				(state.fnum_lsb[channel as usize] & 0xF).into();

			if state.fnum_1[channel as usize] != state.fnum_2[channel as usize] {
				state.midi_wheel[channel as usize] = (state.fnum_2
					[channel as usize] * 0x400)
					.try_into()
					.unwrap(); //(0x4000 / 0x10)
				midi.event_write(Midi {
					channel: midi_channel.into(),
					message: PitchBend {
						bend: midly::PitchBend(
							state.midi_wheel[channel as usize].into(),
						),
					},
				});
			}
		},
		APU_WRE1 => {
			midi.event_write(Midi {
				channel: midi_channel.into(),
				message: NoteOn {
					key: (data & 0x7F).into(),
					vel: 0x7F.into(),
				},
			});
			midi.event_write(Midi {
				channel: midi_channel.into(),
				message: NoteOff {
					key: (data & 0x7F).into(),
					vel: 0x00.into(),
				},
			});
		},
		APU_WRE2 => {
			state.fnum_msb[channel as usize] = data;

			state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
			state.note_2[channel as usize] =
				(state.fnum_msb[channel as usize] & 0x7F).into();
			// note is activated by setting the DAC bit in APU_SMASK
		},
		APU_SMASK => {
			for channel in 0..=0x4 {
				let temp_byte = 2 ^ channel;
				midi_channel = if channel == 0x4 {
					CHN_DAC
				} else {
					MIDI_CHANNEL_SN76489_BASE + channel
				};
				state.note_on_1[channel as usize] =
					state.note_on_2[channel as usize];
				state.note_on_2[channel as usize] = data & temp_byte;

				state.note_en_1[channel as usize] =
					state.note_en_2[channel as usize];
				state.note_en_2[channel as usize] = (state.note_en_2
					[channel as usize] & !0x1)
					| (state.note_on_2[channel as usize]) & 0x1;
				if state.note_en_1[channel as usize]
					!= state.note_en_2[channel as usize]
				{
					if (state.note_en_2[channel as usize] & 0x7) == 0x7 {
						do_note_on(
							state.note_2[channel as usize],
							state.note_2[channel as usize],
							midi_channel,
							&mut state.midi_note[channel as usize],
							&mut state.midi_wheel[channel as usize],
							Some(255),
							None,
							config,
							midi,
						);
						state.note_delay[channel as usize] = 0;
					} else if state.midi_note[channel as usize] != 0xFF {
						// Note got silenced via Channel Mask
						midi.event_write(Midi {
							channel: midi_channel.into(),
							message: NoteOff {
								key: state.midi_note
									[channel as usize]
									.into(),
								vel: 0x00.into(),
							},
						});
						state.midi_note[channel as usize] = 0xFF;
						state.note_delay[channel as usize] = 10000;
					}
				}
			}
		},
		_ => panic!(),
	}
}
