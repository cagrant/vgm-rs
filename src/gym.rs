use crate::midi_shim::MIDIShim;
use crate::sn76489::{SN76489, SN76489_TONE_1, SN76489_TONE_2, SN76489_TONE_3};
use crate::vgm2mid::mid_data_init;
use crate::ym2612::YM2612;
use crate::Config;
use anyhow::{bail, Result};

struct GYMHeader {
	str_gymx: [u8; 4],
}

fn parse_gym_file_header(file_data: &[u8]) -> Result<Option<GYMHeader>> {
	if file_data.len() < 428 {
		return Ok(None);
	}

	let mut header = GYMHeader { str_gymx: [0; 4] };

	if &file_data[0..4] != "GYMX".as_bytes() {
		return Ok(None);
	} else {
		header.str_gymx.clone_from_slice(&file_data[0..4]);
	}

	Ok(Some(header))
}

pub(crate) fn convert_gym_to_mid(file_data: &[u8], config: &Config) -> Result<()> {
	let _file_header = parse_gym_file_header(file_data)?;

	let mut file_pos = 0_usize;
	let register = 0_u8;
	let mut sn76489_msb = 0_u8;
	let mut sn76489_lsb = 0_u8;

	// GYM Command Constants
	const DELAY_166MS: u8 = 0;
	const YM2612_PORT0: u8 = 1;
	const YM2612_PORT1: u8 = 2;
	const YM2612_PSG: u8 = 3;
	let mut _conversion_status_current = 0;
	let _conversion_status_total = file_data.len();

	let _mid_trackdata: Vec<u8> = Vec::new();
	let _mid_file_pos = 0;

	let clock_sn76489 = 3579545;
	let clock_ym2612 = 3579545;
	let fsam_2612 = clock_ym2612 as f64 / 72.0;

	let mut ym2612 = YM2612::new(config, None);
	let mut sn76489 = SN76489::new(config, None);
	let mut midi = MIDIShim::new(config)?;

	let t6w28_sn76489 = false;

	mid_data_init(config, &mut midi, true, t6w28_sn76489);

	//DAC_Pos = 0
	//ReDim DAC_Data(0)
	//let DelayTime: i64
	//let Ttime: f64;
	//let Ttime: f64 = 0;

	loop {
		let switch = file_data[file_pos];
		match switch {
			DELAY_166MS => {
				midi.delta_time += 735;
				for channel in 0x0..=0x2 {
					sn76489.state.note_delay[channel] =
						sn76489.state.note_delay[channel] + 735;
				}
				file_pos = file_pos + 1;

				//DelayTime = DelayTime + 1
				//if DelayTime > 1 & DAC_Pos > 0 {
				//	Call DAC_Write
				//}
			},
			YM2612_PORT0 | YM2612_PORT1 => {
				let port = match switch {
					YM2612_PORT0 => 0,
					YM2612_PORT1 => 3,
					_ => bail!("Pattern matched to a number other YM2612_PORT0 or YM2612_PORT1. This should never occur.")
				};
				let data = file_data[file_pos + 2];
				//if bytRegister != YM2612_DAC { _
				//
				ym2612.command_handle(port, register, data, fsam_2612, &mut midi);
				file_pos = file_pos + 3
				//if bytRegister = YM2612_DAC {
				//	ReDim Preserve DAC_Data(DAC_Pos)
				//	DAC_Data(DAC_Pos) = bytData
				//	DAC_Pos = DAC_Pos + 1
				//	DelayTime = 0
				//}
			},
			YM2612_PSG => {
				sn76489_msb = file_data[file_pos + 1];
				match sn76489_msb & 240 {
					SN76489_TONE_1 | SN76489_TONE_2 | SN76489_TONE_3 => {
						sn76489_lsb = file_data[file_pos + 3];
						file_pos = file_pos + 4;
					},
					_ => file_pos = file_pos + 2,
				}
				sn76489.command_handle(
					sn76489_msb,
					sn76489_lsb,
					clock_sn76489,
					t6w28_sn76489,
					&mut midi,
				);
			},
			_ => file_pos = file_pos + 1,
		}

		_conversion_status_current = file_pos;

		/*if Ttime < Timer {
			Call frmMain.tmrConversionStatus_Timer;
			DoEvents;
			Ttime = Timer + 0.5;
		}*/

		//Loop Until lngFilePos >= UBound(bytGYMFileData) / 8
		if file_pos >= file_data.len() {
			break;
		}
	}
	Ok(())
}
