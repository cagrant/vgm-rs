use crate::midi_shim::{MIDIShim, MIDI_VOLUME};
use crate::vgm2mid::{do_note_on, note};
use crate::Config;
use midly::MidiMessage::{Controller, NoteOff};
use midly::TrackEventKind::Midi;

const AY_AFINE: u8 = 0x00;
const AY_ACOARSE: u8 = 0x01;
const AY_BFINE: u8 = 0x02;
const AY_BCOARSE: u8 = 0x03;
const AY_CFINE: u8 = 0x04;
const AY_CCOARSE: u8 = 0x05;
const AY_NOISEPER: u8 = 0x06;
const AY_ENABLE: u8 = 0x07;
const AY_AVOL: u8 = 0x08;
const AY_BVOL: u8 = 0x09;
const AY_CVOL: u8 = 0x0A;
const AY_EFINE: u8 = 0x0B;
const AY_ECOARSE: u8 = 0x0C;
const AY_ESHAPE: u8 = 0x0D;

const AY_PORTA: u8 = 0x0E;
const AY_PORTB: u8 = 0x0F;

pub(crate) const MIDI_CHANNEL_PSG_BASE: u8 = 0x0A;

//Public AY_NoteDelay(0x0..=0x7) As Long	// used for Note On/Off-Detection
//Public AY_LastVol(0x0..=0x7) As Byte

fn hz_ay(fnum: f64, clock: u32) -> f64 {
	if fnum == 0.0 {
		0.0
	} else {
		f64::from(clock / 16) / fnum
	}
}

#[allow(dead_code)]
pub(crate) struct AY8910State {
	fnum_lsb: [u16; 7],
	fnum_msb: [u8; 7],
	block: [u8; 7],
	fnum_1: [u32; 7],
	fnum_2: [u32; 7],
	hz_1: [f64; 7],
	hz_2: [f64; 7],
	note_1: [f64; 7],
	note_2: [f64; 7],
	feedback: [u8; 2],
	clock_source: [u8; 2],
	attenuation_1: [u8; 7],
	attenuation_2: [u8; 7],
	midi_instr: [u8; 7],
	midi_note: [u8; 7],
	midi_wheel: [u16; 7],
	midi_volume: u8,
	midi_pan: [u8; 7],
	midi_mod: [u8; 7],
	note_on_1: [u8; 7],
	note_on_2: [u8; 7],
}

pub(crate) struct AY8910 {
	state: AY8910State,
	config: Config,
}

impl AY8910 {
	pub(crate) fn new(config: &Config, opt_state: Option<AY8910State>) -> Self {
		AY8910 {
			state: opt_state.unwrap_or(Default::default()),
			config: config.to_owned(),
		}
	}

	//234567890123456789012345678901234567890123456789012345678901234567890123456789012345678
	//0000000111s11111112222222222333333333344444444445555555555666666666666777777778888888888
	pub(crate) fn command_handle(
		&mut self,
		chip_id: u8,
		reg: u8,
		data: u8,
		clock: u32,
		midi: &mut MIDIShim,
	) {
		/*if Variables_Clear_PSG == 1 {
			Erase FNum_1: Erase FNum_2: Erase Hz_1: Erase Hz_2: Erase Note_1: Erase Note_2
			Erase Attenuation_1: Erase Attenuation_2
			Erase MIDINote: Erase MIDIWheel: Erase NoteOn_1: Erase NoteOn_2: MIDIVolume = 0
			for CH in 0x0..=0x7 {
				state.attenuation_2[channel as usize] = 0xFF
				SN76489_NOTE_DELAY[channel as usize] = 0
				//PSG_LastVol[channel as usize] = 0x0
			}
			Variables_Clear_PSG = 0
		}*/

		//if CH < 0x3 {
		//	if SN76489_CH_DISABLED[channel as usize] = 1 { return }
		//} else { //if CH = 0x3 {
		//	if SN76489_NOISE_DISABLED = 1 { return }
		//}

		match reg {
			AY_AFINE | AY_ACOARSE | AY_BFINE | AY_BCOARSE | AY_CFINE | AY_CCOARSE => {
				process_tuning(
					reg,
					chip_id,
					&mut self.state,
					data,
					clock,
					&self.config,
					midi,
				);
			},
			AY_AVOL | AY_BVOL | AY_CVOL => {
				process_vol(reg, chip_id, data, &mut self.state, midi);
			},
			AY_ENABLE => {
				process_enable(data, chip_id, &mut self.state, &self.config, midi);
			},
			AY_NOISEPER => (),
			// Noise Frequency
			AY_EFINE | AY_ECOARSE => (),
			AY_ESHAPE => (),
			AY_PORTA | AY_PORTB => (),
			// ignore
			_ => panic!(),
		}
	}
}

impl Default for AY8910State {
	fn default() -> Self {
		AY8910State {
			fnum_lsb: [0; 7],
			fnum_msb: [0; 7],
			block: [0; 7],
			fnum_1: [0; 7],
			fnum_2: [0; 7],
			hz_1: [0.0; 7],
			hz_2: [0.0; 7],
			note_1: [0xFF.into(); 7],
			note_2: [0xFF.into(); 7],
			feedback: [0; 2],
			clock_source: [0; 2],
			attenuation_1: [0; 7],
			attenuation_2: [0xFF; 7],
			midi_instr: [0; 7],
			midi_note: [0xFF; 7],
			midi_wheel: [0x8000; 7],
			midi_volume: 0,
			midi_pan: [0; 7],
			midi_mod: [0; 7],
			note_on_1: [0; 7],
			note_on_2: [0; 7],
		}
	}
}

fn process_enable(
	data: u8,
	chip_id: u8,
	state: &mut AY8910State,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let mut temp_byte = !data;
	for channel in 0x0..=0x5 {
		let current_channel = chip_id * 0x6 + channel;
		state.note_on_1[current_channel as usize] =
			state.note_on_2[current_channel as usize];
		state.note_on_2[current_channel as usize] = temp_byte & 0x1;
		temp_byte = temp_byte / 0x2;

		if state.note_on_1[current_channel as usize]
			!= state.note_on_2[current_channel as usize]
		{
			let midi_channel = if chip_id != 0 {
				0x0
			} else {
				MIDI_CHANNEL_PSG_BASE
			} + channel;
			if state.note_on_2[current_channel as usize] != 0 {
				do_note_on(
					state.note_2[current_channel as usize],
					state.note_2[current_channel as usize],
					midi_channel,
					&mut state.midi_note[current_channel as usize],
					&mut state.midi_wheel[current_channel as usize],
					Some(255),
					None,
					config,
					midi,
				);
			} else if state.midi_note[current_channel as usize] != 0xFF {
				midi.event_write(Midi {
					channel: midi_channel.into(),
					message: NoteOff {
						key: state.midi_note[current_channel as usize]
							.into(),
						vel: 0x00.into(),
					},
				});
				state.midi_note[current_channel as usize] = 0xFF
			}
		}
	}
}

fn process_vol(reg: u8, chip_id: u8, data: u8, state: &mut AY8910State, midi: &mut MIDIShim) {
	let channel = reg - AY_AVOL;
	let current_channel = chip_id * 0x3 + channel;
	let midi_channel = if chip_id != 0 {
		0x0
	} else {
		MIDI_CHANNEL_PSG_BASE
	} + channel;

	if data & 0x10 != 0 {
		// use Envelope data
		state.attenuation_2[current_channel as usize] = 0xFF;
		state.midi_volume = 0x7F;
	} else {
		state.attenuation_2[current_channel as usize] = data & 0xF;
		state.midi_volume = state.attenuation_2[current_channel as usize] * 0x8;
	}
	midi.event_write(Midi {
		channel: midi_channel.into(),
		message: Controller {
			controller: MIDI_VOLUME,
			value: state.midi_volume.into(),
		},
	});
}

fn process_tuning(
	reg: u8,
	chip_id: u8,
	state: &mut AY8910State,
	data: u8,
	clock: u32,
	config: &Config,
	midi: &mut MIDIShim,
) {
	let channel = reg / 0x2;
	let current_channel = chip_id * 0x3 + channel;
	let midi_channel = if chip_id != 0 {
		0x0
	} else {
		MIDI_CHANNEL_PSG_BASE
	} + channel;

	state.fnum_1[channel as usize] = state.fnum_2[channel as usize];
	if reg & 0x1 != 0 {
		// AY_xCOARSE
		state.fnum_2[current_channel as usize] = (state.fnum_2[current_channel as usize]
			& 0xFF) | ((data as u32 & 0xF) << 8);
	} else {
		// AY_xFINE
		state.fnum_2[current_channel as usize] =
			(state.fnum_2[current_channel as usize] & 0xF00) | (data * 0x1) as u32;
	}

	state.hz_1[current_channel as usize] = state.hz_2[current_channel as usize];
	state.hz_2[current_channel as usize] =
		hz_ay(state.fnum_2[current_channel as usize].into(), clock);

	state.note_1[current_channel as usize] = state.note_2[current_channel as usize];
	let mut temp_note = note(state.hz_2[current_channel as usize]);
	if temp_note >= 0x80.into() {
		//TempNote = 0x7F - state.fnum_2[channel as usize]
		temp_note = 0x7F.into()
	}
	state.note_2[current_channel as usize] = temp_note;

	if state.note_1[current_channel as usize] != state.note_2[current_channel as usize] {
		do_note_on(
			state.note_1[current_channel as usize],
			state.note_2[current_channel as usize],
			midi_channel,
			&mut state.midi_note[current_channel as usize],
			&mut state.midi_wheel[current_channel as usize],
			None,
			None,
			config,
			midi,
		);
	}
}

#[allow(dead_code)]
pub(crate) fn ay_vol_to_db(tl: u8) -> f32 {
	if tl > 0x0 {
		(tl - 0xF).into()
	} else {
		-400.0 // results in volume 0
	}
}
